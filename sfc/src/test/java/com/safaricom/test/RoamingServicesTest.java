package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.services.RoamingPurchaseConfirmationPage;
import com.safaricom.pages.services.RoamingPurchaseFinalConfirmationPage;
import com.safaricom.pages.services.RoamingPurchasePage;
import com.safaricom.pages.services.RoamingServicesPage;
import com.safaricom.pages.services.RoamingTariffPage;
import com.safaricom.pages.sfcHome.ServicesHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class RoamingServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public RoamingServicesPage roamingServicesPageObject=new RoamingServicesPage(driver);
	public RoamingPurchasePage roamingPurchasePageObject=new RoamingPurchasePage(driver);
	public RoamingPurchaseConfirmationPage  roamingPurchaseConfirmationPageObject=new RoamingPurchaseConfirmationPage(driver);
	public RoamingPurchaseFinalConfirmationPage roamingPurchaseFinalConfirmationPageObject=new RoamingPurchaseFinalConfirmationPage(driver);
	public RoamingTariffPage roamingtariffpageObject=new RoamingTariffPage(driver);
	
	
	/*Verfiy whether there is option Purchase and Roaming Tariff in home Page and the title is "ROAMING SERVICES" */
	
	@Test
	public void ROAMINGSERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(50000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.roamingservicesClick)).click();
		}
		String expected_maintitle = "ROAMING SERVICES";
		String actual_maintitle = roamingServicesPageObject.roamingServices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, roamingServicesPageObject.tv_roamign_service_title.isDisplayed(), "Roaming service  title is not displayed");
		Assert.assertEquals(true, roamingServicesPageObject.tv_date_airtime.isDisplayed(), "Airtime Date is not displayed");
		Assert.assertEquals(true, roamingServicesPageObject.tv_amount_roaming.isDisplayed(), "Airtime Amount is not displayed");
		Assert.assertEquals(true, roamingServicesPageObject.roamingpurchase_click.isDisplayed(), "Airtime Sambaza button is not displayed");
		Assert.assertEquals(true, roamingServicesPageObject.roamingtraiff_Click.isDisplayed(), "Data Balance title is not displayed");
		
		Thread.sleep(50000);
			
	}

	/*Verify whether Roaming Bundles Purchase is completed with "Purchase".*/
	public void ROAMINGSERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(50000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.roamingservicesClick)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(roamingServicesPageObject.roamingpurchase_click)).click();
		Thread.sleep(50000);
		String expected_maintitle = "Roaming Bundles";
		String actual_maintitle = roamingPurchasePageObject.roamingbundle_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, roamingPurchasePageObject.tv_amount_roaming.isDisplayed(), "Roaming Amount is not displayed");
		Assert.assertEquals(true, roamingPurchasePageObject.tv_date_airtime.isDisplayed(), "Airtime Date is not displayed");
		Assert.assertEquals(true, roamingPurchasePageObject.btn_roaming_purchase.isDisplayed(), "Purhase button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(roamingPurchasePageObject.btn_roaming_purchase)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingPurchaseConfirmationPageObject.tv_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingPurchaseFinalConfirmationPageObject.tv_ok)).click();
		Thread.sleep(50000);
	}

	/*Verify whether Roaming Tariff gives details on Roaming Tariff*/
	public void ROAMINGSERVICES_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.roamingservicesClick)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(roamingServicesPageObject.roamingtraiff_Click)).click();
		Thread.sleep(50000);
		String expected_maintitle = "Roaming Tariff";
		String actual_maintitle = roamingtariffpageObject.roaming_tariff_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(roamingtariffpageObject.roaming_traiff_continent_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingtariffpageObject.roaming_traiff_continent)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingtariffpageObject.roaming_traiff_country_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingtariffpageObject.roaming_traiff_country)).click();
		wait.until(ExpectedConditions.elementToBeClickable(roamingtariffpageObject.btn_ok)).click();
		Thread.sleep(50000);
	
	}
}
