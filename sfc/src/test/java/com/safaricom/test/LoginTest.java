package com.safaricom.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class LoginTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	File csv_path = new File(System.getProperty("user.dir") + "/sfc.csv");
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);

	/*
	 * Verify whether user is able to login when provided with valid user name and
	 * password and accepted "Terms & Conditions".
	 */

	@Test
	public void LOGIN_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(locationPermissionObject.locationpermission)).click();
		wait.until(ExpectedConditions.elementToBeClickable(managePhonePermissionObject.ManagePhonepermission)).click();
//		wait.until(ExpectedConditions.elementToBeClickable(loginGeneratePinObject.accountNumber)).click();
		// Csv Reader

	CSVReader reader = new CSVReader(new FileReader(csv_path));
		String[] csvCell;
		// while loop will be executed till the last line In CSV.
		while ((csvCell = reader.readNext()) != null) {
			String segment = csvCell[0];
			String msisdn = csvCell[1];
			String amount = csvCell[2];
			String id = csvCell[3];
			if (segment.equals("platinum")) {
				String msidnSegment = csvCell[1];
//				wait.until(ExpectedConditions.elementToBeClickable(loginGeneratePinObject.accountNumber1)).click();
				Thread.sleep(5000);
				loginGeneratePinObject.accountNumber1.clear();
				wait.until(ExpectedConditions.elementToBeClickable(loginGeneratePinObject.accountNumber1)).sendKeys(msidnSegment);
				

				wait.until(ExpectedConditions.elementToBeClickable(loginGeneratePinObject.submitButton1)).click();

				wait.until(ExpectedConditions.elementToBeClickable(loginSuccessPageObject.otp1)).sendKeys("1234");
				
				wait.until(ExpectedConditions.elementToBeClickable(loginSuccessPageObject.termConditionClick1)).click();
				wait.until(ExpectedConditions.elementToBeClickable(loginSuccessPageObject.OtpLoginButton1)).click();
				Thread.sleep(10000);
				String expectedtitle = "MENU";
				String sfctitle = sfcHomePageObject.sfc_title.getText();
				Assert.assertEquals(sfctitle, expectedtitle);
	
			}
		}
	}
}
