package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.contactUS.ContactUS_NewRequest;
import com.safaricom.pages.contactUS.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.sfcHome.ContactUSHomePage;
import com.safaricom.pages.sfcHome.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class ContactUsTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	
	/* Verify whether there is option to create new Request. Able to connect with Youtube,Blog,Google etc. Title should be "CONTACT US" */
	
	@Test
	public void CONTACTUS_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.contactUs_click)).click();
		}
		String expected_maintitle = "CONTACT US";
		String actual_maintitle = contactUshomePageObject.txt_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, contactUshomePageObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_message_click.isDisplayed(), "Message is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.new_request_click.isDisplayed(), "New Request button is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_youtube_click.isDisplayed(), "Youtube is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_blog_click.isDisplayed(), "Blog is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_google_plus_click.isDisplayed(), "Google Plus is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(contactUshomePageObject.new_request_click)).click();
		
		String expected_subtitle = "REQUEST";
		String actual_subtitle = contactUS_NewRequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.requst_msg)).sendKeys("New Request");
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestFinalConfirmationObject.tv_ok)).click();
		
		
	}

}
