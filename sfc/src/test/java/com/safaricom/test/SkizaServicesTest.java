package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.services.SkizaservicesConfirmationPage;
import com.safaricom.pages.services.SkizaservicesPage;
import com.safaricom.pages.sfcHome.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class SkizaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public SkizaservicesPage skizaservicesPageObject=new SkizaservicesPage(driver);
	public SkizaservicesConfirmationPage skizaservicesConfirmationPage=new SkizaservicesConfirmationPage(driver);
	

	/* Verify whether there is option to search Skiza tunes with Artist and Song and the title should be "SKIZA SERVICES". */
	
	@Test
	public void SKIZASERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.skizaservicesClick)).click();
		String expected_maintitle = "SKIZA SERVICES";
		String actual_maintitle = skizaservicesPageObject.skizaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		
		Assert.assertEquals(true, skizaservicesPageObject.rbartist_click.isDisplayed(), "Artist Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.rbsong_click.isDisplayed(), "Song Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.artist_song.isDisplayed(), "Artist/Song field is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.search_btn_Click.isDisplayed(), "Search button is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.rbartist_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.artist_song)).sendKeys("A R RAHMAN");
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.search_btn_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesConfirmationPage.tv_ok)).click();
		
	}

}
