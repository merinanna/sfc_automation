package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.MPesaMyaccountCostCalculatorHomePage;
import com.safaricom.pages.mpesa.MPesaMyaccountCostCalculatorLipanaMpesa;
import com.safaricom.pages.mpesa.MPesaMyaccountCostCalculatorSendWithdraw;
import com.safaricom.pages.mpesa.MPesaMyaccountHomePage;
import com.safaricom.pages.mpesa.MPesaMyaccountMPESA1Tap;
import com.safaricom.pages.mpesa.MPesaMyaccountMpesaUnlock;
import com.safaricom.pages.mpesa.MPesaMyaccountMpesaUnlockConfirmation;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MPESAMyAccountTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public MpesaHomePage mpesahomepageObject = new MpesaHomePage(driver);
	public MPesaMyaccountHomePage mpesamyaccounthomeObject = new MPesaMyaccountHomePage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MPesaMyaccountCostCalculatorHomePage mpesaMyaccountCostCalculatorHomePageObject = new MPesaMyaccountCostCalculatorHomePage(
			driver);
	public MPesaMyaccountCostCalculatorSendWithdraw mpesaMyaccountCostCalculatorSendWithdrawObject = new MPesaMyaccountCostCalculatorSendWithdraw(
			driver);
	public MPesaMyaccountCostCalculatorLipanaMpesa mpesaMyaccountCostCalculatorLipanaMPesaObject = new MPesaMyaccountCostCalculatorLipanaMpesa(
			driver);
	public MPesaMyaccountMPESA1Tap mpesaMyaccountMPESA1TapObject = new MPesaMyaccountMPESA1Tap(driver);
	public MPesaMyaccountMpesaUnlock mpesaMyaccountMpesaUnlockObject = new MPesaMyaccountMpesaUnlock(driver);
	public MPesaMyaccountMpesaUnlockConfirmation mpesaMyaccountMpesaUnlockConfirmationObject = new MPesaMyaccountMpesaUnlockConfirmation(
			driver);

	/*
	 * Verify whether the elements "COST CALCULATOR,M-PESA 1 TAP,M-PESA UNLOCK" are
	 * present in My Account Home Page and Title should be "MY ACCOUNT"
	 */

	@Test
	public void MPESAMYACCOUNT_TC_001() throws InterruptedException, IOException {
		
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
				
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.myaccountClick)).click();
		String expectedtitle = "MY ACCOUNT";
		String myaccount_title = mpesamyaccounthomeObject.myaccount_txt_title.getText();
		Assert.assertEquals(myaccount_title, expectedtitle);
		Assert.assertEquals(true, mpesamyaccounthomeObject.costcalculator_Click.isDisplayed(),"Cost calculator is not displayed");
		Assert.assertEquals(true, mpesamyaccounthomeObject.mpesa1tap_Click.isDisplayed(),"MPesa1tap is not displayed");
		Assert.assertEquals(true, mpesamyaccounthomeObject.mpesaunlock_Click.isDisplayed(),"MpesaUnlock is not displayed");

	}
	/*
	 * Verify whether the "Send Withdrawal" and "Lipa Na MPesa" Cost Calculator
	 * positive flow is completed and the home page title should be
	 * "COST CALCULATOR"
	 */

	@Test
	public void MPESAMYACCOUNT_TC_002() throws InterruptedException, IOException {
	
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.myaccountClick)).click();
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesamyaccounthomeObject.costcalculator_Click)).click();
		Thread.sleep(1000);
		String expectedtitle = "COST CALCULATOR";
		String costcalculator_title = mpesaMyaccountCostCalculatorHomePageObject.costcalculator_title.getText();
		Assert.assertEquals(costcalculator_title, expectedtitle);
		Assert.assertEquals(true, mpesaMyaccountCostCalculatorHomePageObject.tvsendWithdrawLabel.isDisplayed(),"Send/Withdraw is not dispayed");
		Assert.assertEquals(true, mpesaMyaccountCostCalculatorHomePageObject.tvMpesaLabel.isDisplayed(),"Mpesa is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorHomePageObject.tvsendWithdrawLabel)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorSendWithdrawObject.edt_send_withdraw_amount)).sendKeys("100");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorHomePageObject.tvMpesaLabel)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorLipanaMPesaObject.edt_bill_num_or_till_num)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorLipanaMPesaObject.edt_bill_or_till_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountCostCalculatorLipanaMPesaObject.tv_check_trans_cost_btn)).click();
		driver.navigate().back();
	}

	/*
	 * Verify whether the elements "GET MY CARD AND ADD SHORTCUT" are present in
	 * M-PESA 1 TAP and Title should be "M-PESA 1 TAP"
	 */

	@Test
	public void MPESAMYACCOUNT_TC_003() throws InterruptedException, IOException {
		
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.myaccountClick)).click();
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesamyaccounthomeObject.mpesa1tap_Click)).click();
		Thread.sleep(1000);
		String expectedtitle = "M-PESA 1 TAP";
		String mpesa1tap_title = mpesaMyaccountMPESA1TapObject.mpesa1tap_title.getText();
		Assert.assertEquals(mpesa1tap_title, expectedtitle);
		Assert.assertEquals(true, mpesaMyaccountMPESA1TapObject.bind_btn_get_card.isDisplayed(),"Get card is not displayed");
		Assert.assertEquals(true, mpesaMyaccountMPESA1TapObject.add_shortcut_button.isDisplayed(),"Add screenshot is not displayed");
		driver.navigate().back();

	}

	/*
	 * Verify whether MPESA Unlock ic completed with valid ID number and the header
	 * should be "UNLOCK YOUR M-PESA ACCOUNT"
	 */

	@Test
	public void MPESAMYACCOUNT_TC_004() throws InterruptedException, IOException {
		
		
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.myaccountClick)).click();
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesamyaccounthomeObject.mpesaunlock_Click)).click();
		Thread.sleep(1000);
		String expectedtitle = "UNLOCK YOUR M-PESA ACCOUNT";
		String mpesaunlock_title = mpesaMyaccountMpesaUnlockObject.mpesaunlock_labelTitle.getText();
		Assert.assertEquals(mpesaunlock_title, expectedtitle);
		Assert.assertEquals(true, mpesaMyaccountMpesaUnlockObject.inputPin.isDisplayed());
		Assert.assertEquals(true, mpesaMyaccountMpesaUnlockObject.buttonPositive.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountMpesaUnlockObject.inputPin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountMpesaUnlockObject.buttonPositive)).click();
		String expectedMessage = "Dear Customer, We have accepted your request, you will be notified via SMS soon";
		String mpesaunlock_finalMessage = mpesaMyaccountMpesaUnlockConfirmationObject.mpesaunlock_labelMessage
				.getText();
		Assert.assertEquals(mpesaunlock_finalMessage, expectedMessage);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaMyaccountMpesaUnlockConfirmationObject.ok_button)).click();
	}

}
