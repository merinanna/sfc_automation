package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.LoanHomePage;
import com.safaricom.pages.mpesa.Loansandsavingshomepage;
import com.safaricom.pages.mpesa.LockSavingsAccountPage;
import com.safaricom.pages.mpesa.LockSavingsAccount_OpenLockAccountPage;
import com.safaricom.pages.mpesa.LockSavingsAccount_SavePage;
import com.safaricom.pages.mpesa.Locksavingsaccount_MinistatementHomePage;
import com.safaricom.pages.mpesa.Locksavingsaccount_OpenLockAccountConfirmationPage;
import com.safaricom.pages.mpesa.Locksavingsaccount_SaveConfirmationPage;
import com.safaricom.pages.mpesa.Locksavingsaccount_WithdrawConfirmationPage;
import com.safaricom.pages.mpesa.Locksavingsaccount_WithdrawFinalConfirmationPage;
import com.safaricom.pages.mpesa.Locksavingsaccount_withdrawPage;
import com.safaricom.pages.mpesa.Locksavinsaccout_SaveFinalConfirmationPage;
import com.safaricom.pages.mpesa.Locksavinsaccout_openlockaccountFinalConfirmationPage;
import com.safaricom.pages.mpesa.MinistatementHomePage;
import com.safaricom.pages.mpesa.Ministatement_DepositHomePage;
import com.safaricom.pages.mpesa.Ministatement_LoanHomePage;
import com.safaricom.pages.mpesa.Ministatement_LockSavingsHomePage;
import com.safaricom.pages.mpesa.MshwariHomePage;
import com.safaricom.pages.mpesa.PayLoanConfirmationPage;
import com.safaricom.pages.mpesa.PayLoanFinalConfirmationPage;
import com.safaricom.pages.mpesa.PayLoanHomePage;
import com.safaricom.pages.mpesa.RequestLoanConfirmationPage;
import com.safaricom.pages.mpesa.RequestLoanFinalConfirmationPage;
import com.safaricom.pages.mpesa.RequestLoanHomePage;
import com.safaricom.pages.mpesa.SendtoMshwariConfirmationPage;
import com.safaricom.pages.mpesa.SendtoMshwariFinalConfirmationPage;
import com.safaricom.pages.mpesa.SendtoMshwariPage;
import com.safaricom.pages.mpesa.WithdrawFromMshwariConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawFromMshwariFinalConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawfromMshwariPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class LoansandSavingsTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public Loansandsavingshomepage loansandsavingshomepageObject = new Loansandsavingshomepage(driver);
	public MshwariHomePage mshwarihomepageObject = new MshwariHomePage(driver);
	public SendtoMshwariPage sendtomshwariObject = new SendtoMshwariPage(driver);
	public SendtoMshwariConfirmationPage sendtoMshwariConfirmationPageObject = new SendtoMshwariConfirmationPage(
			driver);
	public SendtoMshwariFinalConfirmationPage sendtoMshwariFinalConfirmationPageObject = new SendtoMshwariFinalConfirmationPage(
			driver);
	public WithdrawfromMshwariPage withdrawmshwariObject = new WithdrawfromMshwariPage(driver);
	public WithdrawFromMshwariConfirmationPage withdrawfromMshwariConfirmationPageObject = new WithdrawFromMshwariConfirmationPage(
			driver);
	public WithdrawFromMshwariFinalConfirmationPage withdrawfromMshwariFinalConfirmationPageObject = new WithdrawFromMshwariFinalConfirmationPage(
			driver);

	public LockSavingsAccountPage locksavingsaccountpageObject = new LockSavingsAccountPage(driver);
	public LockSavingsAccount_OpenLockAccountPage OpenLockAccountPageObject = new LockSavingsAccount_OpenLockAccountPage(
			driver);
	public Locksavingsaccount_OpenLockAccountConfirmationPage OpenLockAccountConfirmationPageObject = new Locksavingsaccount_OpenLockAccountConfirmationPage(
			driver);
	public Locksavinsaccout_openlockaccountFinalConfirmationPage openlockaccountFinalConfirmationPageObject = new Locksavinsaccout_openlockaccountFinalConfirmationPage(
			driver);
	public LockSavingsAccount_SavePage savePageObject = new LockSavingsAccount_SavePage(driver);
	public Locksavingsaccount_SaveConfirmationPage saveConfirmationPageObject = new Locksavingsaccount_SaveConfirmationPage(
			driver);
	public Locksavinsaccout_SaveFinalConfirmationPage saveFinalConfirmationPageObject = new Locksavinsaccout_SaveFinalConfirmationPage(
			driver);
	public Locksavingsaccount_withdrawPage withdrawPageObject = new Locksavingsaccount_withdrawPage(driver);
	public Locksavingsaccount_WithdrawConfirmationPage withdrawConfirmationObject = new Locksavingsaccount_WithdrawConfirmationPage(
			driver);
	public Locksavingsaccount_WithdrawFinalConfirmationPage withdrawFinalConfirmationObject = new Locksavingsaccount_WithdrawFinalConfirmationPage(
			driver);
	public Locksavingsaccount_MinistatementHomePage ministatementPageObject = new Locksavingsaccount_MinistatementHomePage(
			driver);
	public LoanHomePage loanhomePageObject = new LoanHomePage(driver);
	public RequestLoanHomePage requestLoanObject = new RequestLoanHomePage(driver);
	public RequestLoanConfirmationPage requestLoanConfirmationObject = new RequestLoanConfirmationPage(driver);
	public RequestLoanFinalConfirmationPage requestLoanFinalConfirmationPageObject = new RequestLoanFinalConfirmationPage(
			driver);
	public PayLoanHomePage payLoanObject = new PayLoanHomePage(driver);
	public PayLoanConfirmationPage payLoanConfirmationObject = new PayLoanConfirmationPage(driver);
	public PayLoanFinalConfirmationPage payLoanFinalConfirmationPageObject = new PayLoanFinalConfirmationPage(driver);
	public MinistatementHomePage ministatementObject = new MinistatementHomePage(driver);
	public Ministatement_DepositHomePage ministatementdepositObject = new Ministatement_DepositHomePage(driver);
	public Ministatement_LoanHomePage ministatementloanObject = new Ministatement_LoanHomePage(driver);
	public Ministatement_LockSavingsHomePage ministatementlocksavingsObject = new Ministatement_LockSavingsHomePage(
			driver);

	/*
	 * Verify whether the elements "MSHWARI,KCB MPESA" are present in Loans and
	 * Savings Home Page and Title should be "LOANS & SAVINGS"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_001() throws InterruptedException, IOException {
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
//			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
		}
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
		String loansandsavingsexpectedtitle = "LOANS & SAVINGS";
		String loansandsavings_title = loansandsavingshomepageObject.loansandsavings_title.getText();
		System.out.println(loansandsavings_title);
		Assert.assertEquals(loansandsavingsexpectedtitle, loansandsavings_title,"Expected: "+loansandsavingsexpectedtitle+"Actual: "+loansandsavings_title);
		Assert.assertEquals(true, loansandsavingshomepageObject.mshwari_Click.isDisplayed(),"M-Shwari is not displayed");
		Assert.assertEquals(true, loansandsavingshomepageObject.kcb_mpesa_click.isDisplayed(),"KCB MPESA is not displayed");

	}

	/*
	 * Verify whether the elements
	 * "SEND TO M-SHWARI,WITHDRAW FROM M-SHWARI,LOCK SAVINGS ACCOUNT,LOAN AND MINISTATEMENT"
	 * are present in Loans and Savings Home Page and Title should be "M-SHWARI"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_002() throws InterruptedException, IOException {
		Thread.sleep(5000);
        
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
		}
		
		if(sfcHomePageObject.title.getText().equals("M-PESA"))
		{
			
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
		}
		
		if(sfcHomePageObject.title.getText().equals("LOANS & SAVINGS"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			
		}
	
		Thread.sleep(1000);
		String mshwariexpectedtitle = "M-SHWARI";
		String mshwari_title = mshwarihomepageObject.mshwari_title.getText();
		System.out.println(mshwari_title);
		Assert.assertEquals(mshwari_title, mshwariexpectedtitle);
		Assert.assertEquals(false, mshwarihomepageObject.textSavings.getText().isEmpty(),"Savings value is empty");
//		Assert.assertEquals(false, mshwarihomepageObject.textLoanAmt.getText().isEmpty(),"Loan value is empty ");
//		Assert.assertEquals(false, mshwarihomepageObject.textDueDate.getText().isEmpty(),"Due Date value is empty" );
//		Assert.assertEquals(false, mshwarihomepageObject.textLoanLimit.getText().isEmpty(),"Loan limit value is empty");
//		Assert.assertEquals(false, mshwarihomepageObject.textTargetAmt.getText().isEmpty(),"Target Amount value is empty");
//		Assert.assertEquals(false, mshwarihomepageObject.textMaturityDate.getText().isEmpty(),"Maturity Date is empty");
		Assert.assertEquals(true, mshwarihomepageObject.sendtomshwari_click.isDisplayed(),"Send to Mshwari is not displayed");
		Assert.assertEquals(true, mshwarihomepageObject.withdrawfrommshwari_click.isDisplayed(),"Withdraw From Mshwari is not displayed");
		Assert.assertEquals(true, mshwarihomepageObject.locksavingsaccount_click.isDisplayed(),"Loan and savings account is not displayed");
		Assert.assertEquals(true, mshwarihomepageObject.loan_click.isDisplayed(),"Loan is not displayed");
		Assert.assertEquals(true, mshwarihomepageObject.ministatement_click.isDisplayed(),"Ministatement is not displayed");

	}

	/*
	 * Verify whether the "Send to M-Shwari" transaction is getting completed with
	 * vaid Amount
	 */

	@Test
	public void LOANSANDSAVINGS_TC_003() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.sendtomshwari_click)).click();
		String sendtomshwariexpectedtitle = "Send to M-Shwari";
		String sendtomshwari_title = sendtomshwariObject.sendtomshwari_title.getText();
		System.out.println(sendtomshwari_title);
		Assert.assertEquals(sendtomshwari_title, sendtomshwariexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(sendtomshwariObject.et_mshwari_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(sendtomshwariObject.btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = sendtoMshwariConfirmationPageObject.sendtomshwari_txt_dialog_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String sendtomshwari_txt_dialog_amount = sendtoMshwariConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, sendtomshwari_txt_dialog_amount.isEmpty(),"Amount field is emoty");
		String sendtomshwari_from_value = sendtoMshwariConfirmationPageObject.from_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_from_value.isEmpty(),"From value field is empty");
		String sendtomshwari_to_value = sendtoMshwariConfirmationPageObject.to_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_to_value.isEmpty(),"To value field is empty");
	
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_cancel_dilaog.isEnabled());
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_continue_dilaog.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariConfirmationPageObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendtoMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);

		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariFinalConfirmationPageObject.ok_button)).click();

	}

	/*
	 * Verify whether the "Withdraw From M-Shwari" transaction is getting completed
	 * with vaid Amount
	 */

	@Test
	public void LOANSANDSAVINGS_TC_004() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.withdrawfrommshwari_click)).click();
		String withdrawfrommshwariexpectedtitle = "Withdraw from\n" + "M-Shwari";
		String withdrawfrommshwari_title = withdrawmshwariObject.withdrawfromshwari_title.getText();
		System.out.println(withdrawfrommshwari_title);
		Assert.assertEquals(withdrawfrommshwari_title, withdrawfrommshwariexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawmshwariObject.wfm_et_mshwari_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawmshwariObject.withdrawfrommshwari_btn_continue)).click();

		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = withdrawfromMshwariConfirmationPageObject.withdrawfrommshwari_txt_dialog_title
				.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String sendtomshwari_txt_dialog_amount = withdrawfromMshwariConfirmationPageObject.withdrawfrommshwari_txt_dialog_amount
				.getText();
		Assert.assertEquals(false, sendtomshwari_txt_dialog_amount.isEmpty(),"Amount field is empty");
		String sendtomshwari_from_value = withdrawfromMshwariConfirmationPageObject.from_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_from_value.isEmpty(),"From value field is empty");
		String sendtomshwari_to_value = withdrawfromMshwariConfirmationPageObject.to_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_to_value.isEmpty(),"To value field is empty");
	
		Assert.assertEquals(true,
				withdrawfromMshwariConfirmationPageObject.withdrawfrommshwari_txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true,
				withdrawfromMshwariConfirmationPageObject.withdrawfrommshwari_txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawfromMshwariConfirmationPageObject.withdrawfrommshwari_txt_continue_dilaog)).click();
		Thread.sleep(10000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = withdrawfromMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);
		Assert.assertEquals(actual_label, expected_label);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawfromMshwariFinalConfirmationPageObject.ok_button)).click();
		

	}

	/*
	 * Verify whether the elements
	 * "OPEN LOCK ACCOUNT,SAVE,WITHDRAW AND MINISTATEMENT" are present in Lock
	 * Savings Account Home Page and Title should be "LOCK SAVINGS ACCOUNT"
	 */

	
	public void LOANSANDSAVINGS_TC_005() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.locksavingsaccount_click)).click();
		String locksavingsaccountexpectedtitle = "LOCK SAVINGS ACCOUNT";
		String locksavingsaccount_title = locksavingsaccountpageObject.locksavingsaccount_title.getText();
		System.out.println(locksavingsaccount_title);
//		Assert.assertEquals(locksavingsaccount_title, locksavingsaccountexpectedtitle);
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_title.getText().isEmpty(),"Account is empty");
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_textSavings.getText().isEmpty(),"Savings is empty");
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_textLoanAmt.getText().isEmpty(),"Loan amount is empty");
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_textDueDate.getText().isEmpty(),"Duedate is empty");
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_textLoanLimit.getText().isEmpty(),"Loan limit is empty");
//		Assert.assertEquals(false, locksavingsaccountpageObject.locksavingsaccount_textTargetAmt.getText().isEmpty(),"Target Amount is empty");
//		Assert.assertEquals(false,
//				locksavingsaccountpageObject.locksavingsaccount_textMaturityDate.getText().isEmpty(),"Maturity date is empty");
		Assert.assertEquals(true, locksavingsaccountpageObject.openlockaccount_click.isDisplayed(),"Open Lock Account is not displayed");
		Assert.assertEquals(true, locksavingsaccountpageObject.save_click.isDisplayed(),"Save is not displayed");
		Assert.assertEquals(true, locksavingsaccountpageObject.withdraw_click.isDisplayed(),"Withdraw is not displayed");
		Assert.assertEquals(true, locksavingsaccountpageObject.ministatement_click.isDisplayed(),"Ministatement is not displayed");
	}

	/*
	 * Verify whether the "Open Account" transaction is getting completed with vaid
	 * Target Amount,Period and Amount and header should be "Open Account"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_006() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		    wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.locksavingsaccount_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(locksavingsaccountpageObject.openlockaccount_click)).click();
		String openlockaccountexpectedtitle = "Open Account";
		String openlockaccoun_title = OpenLockAccountPageObject.openlockaccount_title.getText();
		System.out.println(openlockaccoun_title);
		Assert.assertEquals(openlockaccoun_title, openlockaccountexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.acc_from_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.mpesa_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.et_target_amount)).sendKeys("1200");
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.et_period)).sendKeys("12");
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.et_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountPageObject.btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation - Open Account";
		String actual_confirmation_title = OpenLockAccountConfirmationPageObject.openlockaccount_txt_dialog_title
				.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		
		
		
		String OpenLockAccount_accountof = OpenLockAccountConfirmationPageObject.accountof.getText();
		Assert.assertEquals(false, OpenLockAccount_accountof.isEmpty(),"Account of is empty");
		String OpenLockAccount_period = OpenLockAccountConfirmationPageObject.period.getText();
		Assert.assertEquals(false, OpenLockAccount_period.isEmpty(),"Period is empty");
		String OpenLockAccount_deposit = OpenLockAccountConfirmationPageObject.deposit.getText();
		Assert.assertEquals(false, OpenLockAccount_deposit.isEmpty(),"Deposit is empty");
		String OpenLockAccount_transaction = OpenLockAccountConfirmationPageObject.transcost.getText();
		Assert.assertEquals(false, OpenLockAccount_transaction.isEmpty(),"Transaction cost is empty");
		
		
		Assert.assertEquals(true, OpenLockAccountConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, OpenLockAccountConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(OpenLockAccountConfirmationPageObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = openlockaccountFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);
		wait.until(ExpectedConditions.elementToBeClickable(openlockaccountFinalConfirmationPageObject.ok_button)).click();

	}

	/*
	 * Verify whether the "Save" transaction is getting completed with vaid Amount
	 * and header should be "Save"
	 */
	@Test
	public void LOANSANDSAVINGS_TC_007() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.locksavingsaccount_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(locksavingsaccountpageObject.save_click)).click();
		String saveexpectedtitle = "Save";
		String save_title = savePageObject.save_title.getText();
		System.out.println(save_title);
		Assert.assertEquals(save_title, saveexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(savePageObject.save_acc_from_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(savePageObject.mpesa_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(savePageObject.save_et_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(savePageObject.btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Save";
		String actual_confirmation_title = saveConfirmationPageObject.save_txt_dialog_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String sendtomshwari_txt_dialog_amount = sendtoMshwariConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, sendtomshwari_txt_dialog_amount.isEmpty(),"Amount field is emoty");
		String sendtomshwari_from_value = sendtoMshwariConfirmationPageObject.from_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_from_value.isEmpty(),"From value field is empty");
		String sendtomshwari_to_value = sendtoMshwariConfirmationPageObject.to_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_to_value.isEmpty(),"To value field is empty");
	
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_cancel_dilaog.isEnabled());
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_continue_dilaog.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariConfirmationPageObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendtoMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);

		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariFinalConfirmationPageObject.ok_button)).click();



	}

	/*
	 * Verify whether the "Withdraw" transaction is getting completed with vaid
	 * Amount and header should be "Withdraw"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_008() throws InterruptedException, IOException {


		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.locksavingsaccount_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(locksavingsaccountpageObject.withdraw_click)).click();
		String withdrawexpectedtitle = "Withdraw";
		String withdraw_title = withdrawPageObject.withdraw_title.getText();
		System.out.println(withdraw_title);
		Assert.assertEquals(withdraw_title, withdrawexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawPageObject.withdraw_et_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawPageObject.btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Withdraw";
		String actual_confirmation_title = withdrawConfirmationObject.withdraw_txt_dialog_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String sendtomshwari_txt_dialog_amount = sendtoMshwariConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, sendtomshwari_txt_dialog_amount.isEmpty(),"Amount field is emoty");
		String sendtomshwari_from_value = sendtoMshwariConfirmationPageObject.from_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_from_value.isEmpty(),"From value field is empty");
		String sendtomshwari_to_value = sendtoMshwariConfirmationPageObject.to_value
				.getText();
		Assert.assertEquals(false, sendtomshwari_to_value.isEmpty(),"To value field is empty");
	
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_cancel_dilaog.isEnabled());
		Assert.assertEquals(true, sendtoMshwariConfirmationPageObject.txt_continue_dilaog.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariConfirmationPageObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendtoMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);

		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariFinalConfirmationPageObject.ok_button)).click();



	}

	/*
	 * Verify whether the Lock Savings Account Ministatement is displayed , Title
	 * should be "MINI STATEMENT" and header should be
	 * "Lock Savings Account Mini Statement"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_009() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.locksavingsaccount_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(locksavingsaccountpageObject.ministatement_click)).click();
		String ministatementexpectedtitle = "MINI STATEMENT";
		String ministatement_title = ministatementPageObject.ministatement_title.getText();
		System.out.println(ministatement_title);
		Assert.assertEquals(ministatement_title, ministatementexpectedtitle);
		String ministatementexpectedheader = "Lock Savings Account Mini Statement"; // Lock Savings Account Mini
																					// Statement
		String ministatement_header = ministatementPageObject.ministatement_tvStmtHeader.getText();
		System.out.println(ministatement_header);
		Assert.assertEquals(ministatement_header, ministatementexpectedheader);
		wait.until(ExpectedConditions.elementToBeClickable(ministatementPageObject.backclick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(ministatementPageObject.backclick)).click();

	}

	/*
	 * Verify whether the elements "REQUEST LOAN AND PAY LOAN" are present in Loan
	 * Home Page and Title should be "LOANS"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_010() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			
		}
		
		if(sfcHomePageObject.title.getText().equals("M-PESA"))
		{
			
			mpesaHomePageObject.loansandsavingsClick.click();
			loansandsavingshomepageObject.mshwari_Click.click();
			
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.loan_click)).click();
		Thread.sleep(5000);
		String expectedtitle = "LOANS";
		String loan_title = loanhomePageObject.loan_title.getText();
		Assert.assertEquals(loan_title, expectedtitle);
		Assert.assertEquals(true, loanhomePageObject.requestLoan_click.isDisplayed(),"Request Loan is not displayed");
		Assert.assertEquals(true, loanhomePageObject.payLoan_click.isDisplayed(),"Pay Loan is not displayed");
//		Assert.assertEquals(true, loanhomePageObject.loan_textSavings.getText().isEmpty(),"Savings is empty");
//		Assert.assertEquals(true, loanhomePageObject.loan_textLoanAmt.getText().isEmpty(),"Loan amount is empty");
//		Assert.assertEquals(true, loanhomePageObject.loan_textDueDate.getText().isEmpty(),"Due date is empty");
//		Assert.assertEquals(true, loanhomePageObject.loan_textLoanLimit.getText().isEmpty(),"Loan limit is empty");
//		Assert.assertEquals(true, loanhomePageObject.loan_textTargetAmt.getText().isEmpty(),"Target Amount is empty");
//		Assert.assertEquals(true, loanhomePageObject.loan_textMaturityDate.getText().isEmpty(),"Maturity date is empty");

	}

	/*
	 * Verify whether the "Request Loan" transaction is getting completed with vaid
	 * Amount and header should be "Request Loan"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_011() throws InterruptedException, IOException {
   
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.loan_click)).click();
		}
		
		if(sfcHomePageObject.title.getText().equals("M-PESA"))
		{
			
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.loan_click)).click();
			
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(loanhomePageObject.requestLoan_click)).click();
		String requestLoanexpectedtitle = "Request Loan";
		String requestLoan_title = requestLoanObject.requestloan_title.getText();
		System.out.println(requestLoan_title);
		Assert.assertEquals(requestLoan_title, requestLoanexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(requestLoanObject.loan_et_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(requestLoanObject.loan_btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Request Loan";
		String actual_confirmation_title = requestLoanConfirmationObject.requestloan_dialog_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String requestLoan_txt_dialog_amount = requestLoanConfirmationObject.requestloan_txt_dialog_amount.getText();
		Assert.assertEquals(false, requestLoan_txt_dialog_amount.isEmpty(),"Dialog amount is empty");
		String requestLoan_period = requestLoanConfirmationObject.requestloan_period.getText();
		Assert.assertEquals(false, requestLoan_period.isEmpty(),"Period is empty");
		String requestLoan_trans = requestLoanConfirmationObject.requestloan_transcost.getText();
		Assert.assertEquals(false, requestLoan_trans.isEmpty(),"Transaction cost is empty");
		
		
		
		Assert.assertEquals(true, requestLoanConfirmationObject.requestloan_txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, requestLoanConfirmationObject.requestloan_txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(requestLoanConfirmationObject.requestloan_txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendtoMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);

		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariFinalConfirmationPageObject.ok_button)).click();

	}

	/*
	 * Verify whether the "Pay Loan" transaction is getting completed with vaid
	 * Amount and header should be "Pay Loan"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_012() throws InterruptedException, IOException {

		Thread.sleep(1000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.loan_click)).click();
		}
		
		if(sfcHomePageObject.title.getText().equals("M-PESA"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.loan_click)).click();
						
		}
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(loanhomePageObject.payLoan_click)).click();
		String payloanexpectedtitle = "Pay Loan";
		String payloan_title = payLoanObject.payloan_title.getText();
		System.out.println(payloan_title);
		Assert.assertEquals(payloan_title, payloanexpectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(payLoanObject.loan_from_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(payLoanObject.mpesa_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(payLoanObject.loan_et_amount)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(payLoanObject.loan_btn_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Pay Loan";
		String actual_confirmation_title = payLoanConfirmationObject.payloan_txt_dialog_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		
		String payloan_txt_dialog_from = payLoanConfirmationObject.payloan_from.getText();
		Assert.assertEquals(false, payloan_txt_dialog_from.isEmpty(),"From is empty");
		
		String payloan_txt_dialog_amount = payLoanConfirmationObject.payloan_amount.getText();
		Assert.assertEquals(false, payloan_txt_dialog_amount.isEmpty(),"Amount is empty");
		String payloan_txt_dialog_trans = payLoanConfirmationObject.payloan_trans.getText();
		Assert.assertEquals(false, payloan_txt_dialog_trans.isEmpty(),"Transaction cost is empty");
		
		
		
		
		Assert.assertEquals(true, payLoanConfirmationObject.payloan_txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, payLoanConfirmationObject.payloan_txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(payLoanConfirmationObject.payloan_txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendtoMshwariFinalConfirmationPageObject.labelMessage.getText();
		System.out.println(actual_label);
		Assert.assertEquals(actual_label, expected_label);

		wait.until(ExpectedConditions.elementToBeClickable(sendtoMshwariFinalConfirmationPageObject.ok_button)).click();


	}

	/*
	 * Verify whether the elements
	 * "DEPOSIT MINI STATEMENT,LOAN MINI STATEMENT AND LOCK SAVINGS ACCOUNT MINI STATEMENT"
	 * are present in Mini Statement Home Page and Title should be "MINI STATEMENT"
	 */

	@Test
	public void LOANSANDSAVINGS_TC_013() throws InterruptedException, IOException {
		
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		
		}
		wait.until(ExpectedConditions.elementToBeClickable(mshwarihomepageObject.ministatement_click)).click();
		String ministatementexpectedtitle = "MINI STATEMENT";
		String ministatement_title = ministatementObject.ministatement_title.getText();
		Assert.assertEquals(ministatement_title, ministatementexpectedtitle);
		
		wait.until(ExpectedConditions.elementToBeClickable(ministatementObject.depositministatement_click)).click();
//		String ministatementdeposit_title = "Deposit Mini Statement";
//		String ministatementdepositexpectedtitle = ministatementdepositObject.depositministatement_header.getText();
		String depositministatementsuccss_message=ministatementdepositObject.succss_message.getText();
		String depositministatementsuccss_message_expected="Please Enter M-PIN";
		wait.until(ExpectedConditions.elementToBeClickable(ministatementdepositObject.btn_cancel)).click();
		wait.until(ExpectedConditions.elementToBeClickable(ministatementObject.loanministatement_click)).click();
		String ministatementloan_title = "Loan Mini Statement";
		String ministatementloanexpectedtitle = ministatementloanObject.loanministatement_header.getText();
		wait.until(ExpectedConditions.elementToBeClickable(ministatementloanObject.back)).click();

		wait.until(ExpectedConditions.elementToBeClickable(ministatementObject.locksavingsaccountministatement_click)).click();
		String ministatementlocksavingsaccount_title = "Lock Savings Account Mini Statement";
		String ministatementlocksavingsexpectedtitle = ministatementlocksavingsObject.locksaviingsministatement_header
				.getText();
		Assert.assertEquals(depositministatementsuccss_message, depositministatementsuccss_message_expected);
		Assert.assertEquals(ministatementloan_title, ministatementloanexpectedtitle);
		Assert.assertEquals(ministatementlocksavingsaccount_title, ministatementlocksavingsexpectedtitle);
	}

	@Test
	public void LOANSANDSAVINGS_TC_014() throws InterruptedException, IOException {

			System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.loansandsavingsClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.mshwari_Click)).click();
		
		}
		wait.until(ExpectedConditions.elementToBeClickable(loansandsavingshomepageObject.kcb_mpesa_click)).click();

	}

}
