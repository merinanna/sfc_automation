package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.sfcHome.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class ServiceHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);

	/*
	 * Verify whether the icons such as
	 * "BONGA SERVICES,M-FLEX,DATA AND SMS PLANS,MY SMS SERVICES,SAMBAZA SERVICES,SKIZA SERVICES,SERVICES,ROAMING SERVICES AND OKOA SERVICES "
	 *  and Title should be "SERVICES"
	 */
	@Test
	public void SERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
				
		if (sfcHomePageObject.title.getText().equals("MENU")) {
		
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			
		}
		String expected_confirmation_title = "SERVICES";
		String actual_confirmation_title = servicesHomePageObject.services_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		Assert.assertEquals(true, servicesHomePageObject.bongaservicesClick.isDisplayed(),"Bonga Services icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.flexClick.isDisplayed(),"Flex icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.dataandsmsplansClick.isDisplayed(),"Data and SMS Plans icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.mysmsservicesClick.isDisplayed(),"My SMS Services icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.sambazaservicesClick.isDisplayed(),"Sambaza Services icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.skizaservicesClick.isDisplayed(),"Skiza Services icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.roamingservicesClick.isDisplayed(),"Roaming Services icon is not displayed");
		Assert.assertEquals(true, servicesHomePageObject.okoaservicesClick.isDisplayed(),"Okoa Services icon is not displayed");
		
	}

}
