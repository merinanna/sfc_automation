package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.services.FlexBuyBundlePage;
import com.safaricom.pages.services.FlexBuyBundle_ConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_FinalConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_PayWith;
import com.safaricom.pages.services.FlexBuyOtherConfirmationPage;
import com.safaricom.pages.services.FlexBuyOtherPage;
import com.safaricom.pages.services.FlexBuyother_FinalConfirmationPage;
import com.safaricom.pages.services.FlexServiceStopAutoRenew_ConfirmationPage;
import com.safaricom.pages.services.Flexservicespage;
import com.safaricom.pages.sfcHome.ServicesHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class FlexServiceTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public Flexservicespage flexservicespageObject = new Flexservicespage(driver);
	public FlexBuyBundlePage flexBuyBundlePageObject = new FlexBuyBundlePage(driver);
	public FlexBuyBundle_PayWith flexbuybundle_paywithObject = new FlexBuyBundle_PayWith(driver);
	public FlexBuyBundle_ConfirmationPage flexbuybundle_confirmationObject = new FlexBuyBundle_ConfirmationPage(driver);
	public FlexBuyBundle_FinalConfirmationPage flexBuyBundle_FinalConfirmationPage = new FlexBuyBundle_FinalConfirmationPage(
			driver);
	public FlexServiceStopAutoRenew_ConfirmationPage stopAutoRenewObject = new FlexServiceStopAutoRenew_ConfirmationPage(
			driver);
	public FlexBuyOtherPage flexbuyOtherObject = new FlexBuyOtherPage(driver);
	public FlexBuyOtherConfirmationPage FlexBuyOtherConfirmationObject = new FlexBuyOtherConfirmationPage(driver);
	public FlexBuyother_FinalConfirmationPage flexbuyOtherFinalConfirmationObject = new FlexBuyother_FinalConfirmationPage(
			driver);

	/*
	 * Verify whether
	 * "BALANCE,MONTHLY FLEX,WEEKLY FLEX,STOP AUTO RENEW,BUY FOR OTHER NUMBER " are
	 * present , expanding weekly and monthly flex should give an option to buy flex
	 * bundle with "BUY BUNDLE" and Title should be "FLEX".
	 */
	@Test
	public void FLEXSERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}
		Thread.sleep(5000);
		String expected_maintitle = "FLEX";
		String actual_maintitle = flexservicespageObject.flex_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);

		Assert.assertEquals(true, flexservicespageObject.tv_flex_balance.isDisplayed(), "Balance  is not displayed");
		Assert.assertEquals(true, flexservicespageObject.monthlyflex_click.isDisplayed(),
				"Monthly Flex is not displayed");

		if (!(flexservicespageObject.buybundleClick.isDisplayed())) {
			flexservicespageObject.monthlyflex_click.click();
		}

	}

	/*
	 * Verify whether it is possible to buy MONTHLY FLEX BUNDLE with "BUY ONCE" and
	 * the transaction is completed
	 */

	@Test
	public void FLEXSERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(5000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}

		if (!(flexservicespageObject.buybundleClick.isDisplayed())) {

			wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.monthlyflex_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.buybundleClick)).click();

		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.buyOnce_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.btn_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.airtime_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.btn_continue)).click();

		String expected_confirmationtitle = "Confirmation";
		String actual_confirmationtitle = flexbuybundle_confirmationObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmationtitle, expected_confirmationtitle);

		Assert.assertEquals(true, flexbuybundle_confirmationObject.confirm_msg.isDisplayed(),
				"Confirmation Message is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvOffer.isDisplayed(), "Offer is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvValidity.isDisplayed(),
				"Validity is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_confirmationObject.btn_continue)).click();

		String expected_finalMessage = "Dear Customer, We have received your request.Thank you for staying with Safaricom.";
		String actual_finalMessage = flexBuyBundle_FinalConfirmationPage.succss_message.getText();
		Assert.assertEquals(actual_finalMessage, expected_finalMessage);
		flexBuyBundle_FinalConfirmationPage.ok_btn.click();

	}
	/*
	 * Verify whether it is possible to buy MONTHLY FLEX BUNDLE with "AUTO RENEW"
	 * and the transaction is completed
	 */

	@Test
	public void FLEXSERVICES_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}

		if (!(flexservicespageObject.buybundleClick.isDisplayed())) {

			wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.monthlyflex_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.buybundleClick)).click();

		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.autoRenew_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.btn_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.airtime_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.btn_continue)).click();

		String expected_confirmationtitle = "Confirmation";
		String actual_confirmationtitle = flexbuybundle_confirmationObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmationtitle, expected_confirmationtitle);

		Assert.assertEquals(true, flexbuybundle_confirmationObject.confirm_msg.isDisplayed(),
				"Confirmation Message is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvOffer.isDisplayed(), "Offer is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvValidity.isDisplayed(),
				"Validity is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_confirmationObject.btn_continue)).click();

		String expected_finalMessage = "Dear Customer, We have received your request.Thank you for staying with Safaricom.";
		String actual_finalMessage = flexBuyBundle_FinalConfirmationPage.succss_message.getText();
		Assert.assertEquals(actual_finalMessage, expected_finalMessage);
		flexBuyBundle_FinalConfirmationPage.ok_btn.click();
	}
	/*
	 * Verify whether it is possible to buy WEEKLY FLEX BUNDLE with "BUY ONCE" and
	 * the transaction is completed
	 */

	@Test
	public void FLEXSERVICES_TC_004() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}
		
		MobileElement weeklyflex_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"WEEKLY FLEX\"))");
		wait.until(ExpectedConditions.elementToBeClickable(weeklyflex_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.buybundleClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.buyOnce_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.btn_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.airtime_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.btn_continue)).click();

		String expected_confirmationtitle = "Confirmation";
		String actual_confirmationtitle = flexbuybundle_confirmationObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmationtitle, expected_confirmationtitle);

		Assert.assertEquals(true, flexbuybundle_confirmationObject.confirm_msg.isDisplayed(),
				"Confirmation Message is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvOffer.isDisplayed(), "Offer is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvValidity.isDisplayed(),
				"Validity is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_confirmationObject.btn_continue)).click();

		String expected_finalMessage = "Dear Customer, We have received your request.Thank you for staying with Safaricom.";
		String actual_finalMessage = flexBuyBundle_FinalConfirmationPage.succss_message.getText();
		Assert.assertEquals(actual_finalMessage, expected_finalMessage);
		flexBuyBundle_FinalConfirmationPage.ok_btn.click();

	}

	/*
	 * Verify whether it is possible to buy WEEKLY FLEX BUNDLE with "AUTO RENEW" and
	 * the transaction is completed
	 */

	@Test
	public void FLEXSERVICES_TC_005() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
			wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.weeklyflex_click)).click();
		}

		MobileElement weeklyflex_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"WEEKLY FLEX\"))");
		wait.until(ExpectedConditions.elementToBeClickable(weeklyflex_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.buybundleClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.autoRenew_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexBuyBundlePageObject.btn_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.airtime_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_paywithObject.btn_continue)).click();

		String expected_confirmationtitle = "Confirmation";
		String actual_confirmationtitle = flexbuybundle_confirmationObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmationtitle, expected_confirmationtitle);

		Assert.assertEquals(true, flexbuybundle_confirmationObject.confirm_msg.isDisplayed(),
				"Confirmation Message is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvOffer.isDisplayed(), "Offer is not displayed");
		Assert.assertEquals(true, flexbuybundle_confirmationObject.tvValidity.isDisplayed(),
				"Validity is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(flexbuybundle_confirmationObject.btn_continue)).click();

		String expected_finalMessage = "Dear Customer, We have received your request.Thank you for staying with Safaricom.";
		String actual_finalMessage = flexBuyBundle_FinalConfirmationPage.succss_message.getText();
		Assert.assertEquals(actual_finalMessage, expected_finalMessage);
		flexBuyBundle_FinalConfirmationPage.ok_btn.click();

	}

	/*
	 * Verify whether it is possible to stop Auto Renew using "STOP AUTO RENEW"
	 * feature
	 */
	@Test
	public void FLEXSERVICES_TC_006() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}

		MobileElement tv_stop_autorenew = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Stop Auto Renew\"))");
		wait.until(ExpectedConditions.elementToBeClickable(tv_stop_autorenew)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(flexservicespageObject.tv_stop_autorenew)).click();
		wait.until(ExpectedConditions.elementToBeClickable(stopAutoRenewObject.tv_ok)).click();

	}

	/*
	 * Verify whether it is possible to Buy Flex bundle for other number using
	 * "BUY FOR OTHER NUMBER" feature
	 */
	@Test
	public void FLEXSERVICES_TC_007() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.flexClick)).click();
		}
	
		MobileElement flex_buy_other = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Buy for other number\"))");
		
		wait.until(ExpectedConditions.elementToBeClickable(flex_buy_other)).click();


		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherObject.edt_mobile)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherObject.spin_flex_bundle)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherObject.flexbundle_select)).click();

		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherObject.airtime_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherObject.btn_continue)).click();

		String expected_confirmationtitle = "Confirmation";
		String actual_confirmationtitle = FlexBuyOtherConfirmationObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmationtitle, expected_confirmationtitle);

		Assert.assertEquals(true, FlexBuyOtherConfirmationObject.confirm_msg.isDisplayed(),
				"Confirmation Message is not displayed");
		Assert.assertEquals(true, FlexBuyOtherConfirmationObject.tvOffer.isDisplayed(), "Offer is not displayed");
		Assert.assertEquals(true, FlexBuyOtherConfirmationObject.tvValidity.isDisplayed(), "Validity is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(FlexBuyOtherConfirmationObject.btn_continue)).click();

		String expected_finalMessage = "Dear Customer, We have received your request.Thank you for staying with Safaricom.";
		String actual_finalMessage = flexbuyOtherFinalConfirmationObject.succss_message.getText();
		Assert.assertEquals(actual_finalMessage, expected_finalMessage);
		wait.until(ExpectedConditions.elementToBeClickable(flexbuyOtherFinalConfirmationObject.ok_btn)).click();
		Thread.sleep(10000);
	}

}
