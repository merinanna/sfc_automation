package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.services.SambazaAirtimeConfirmationPage;
import com.safaricom.pages.services.SambazaAirtimePage;
import com.safaricom.pages.services.SambazaDataConfirmationPage;
import com.safaricom.pages.services.SambazaDataPage;
import com.safaricom.pages.services.SambazaservicesPage;
import com.safaricom.pages.sfcHome.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class SambazaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public SambazaservicesPage sambazaservicesObject=new SambazaservicesPage(driver);
	public SambazaAirtimePage sambazaAirtimePageObject=new SambazaAirtimePage(driver);
	public SambazaAirtimeConfirmationPage  sambazaAirtimeConfirmationPageObject=new SambazaAirtimeConfirmationPage(driver);
	public SambazaDataPage sambazaDataPageObject=new SambazaDataPage(driver);
	public SambazaDataConfirmationPage  sambazaDataConfirmationPageObject=new SambazaDataConfirmationPage(driver);

	/* Verify whether Airtime Balance Sambaza,Data Balance Sambaza is availbale in Sambaza Services Home Page and the title is "SAMBAZA SERVICES" */
	
	@Test
	public void SAMBAZASERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.sambazaservicesClick)).click();
		}
		String expected_maintitle = "SAMBAZA SERVICES";
		String actual_maintitle = sambazaservicesObject.sambazaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, sambazaservicesObject.airtime_tv_bonga_points_title.isDisplayed(), "Airtime Balance title is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.tv_date_airtime.isDisplayed(), "Airtime Date is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.tv_amount_airtime.isDisplayed(), "Airtime Amount is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.airtimesambazaClick.isDisplayed(), "Airtime Sambaza button is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.tv_bonga_points_data.isDisplayed(), "Data Balance title is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.tv_date_data.isDisplayed(), "Data Date is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.tv_amount_data.isDisplayed(), "Data Amount is not displayed");
		Assert.assertEquals(true, sambazaservicesObject.datasambazaClick.isDisplayed(), "Data Sambaza button is not displayed");
		
		
	}

	/*Verify whether Airtime Balance Sambaza transaction is completed with valid Recipient's Mobile Number and Airtime to be transferred.*/
	
	@Test
	public void SAMBAZASERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.sambazaservicesClick)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(sambazaservicesObject.airtimesambazaClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazaAirtimePageObject.airtime_edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaAirtimePageObject.airtime_et_pin)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaAirtimePageObject.airtime_btn_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazaAirtimeConfirmationPageObject.airtime_tv_ok)).click();
		
	}
	
/*Verify whether Data Balance Sambaza transaction is completed with valid Recipient's Mobile Number and Data to be transferred.*/
	
	@Test
	public void SAMBAZASERVICES_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.sambazaservicesClick)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(sambazaservicesObject.datasambazaClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazaDataPageObject.data_edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaDataPageObject.data_et_pin)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaDataPageObject.data_btn_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazaDataConfirmationPageObject.data_tv_ok)).click();
		driver.navigate().back();
	}
	
}
