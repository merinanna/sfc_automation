package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.contactUS.ContactUS_NewRequest;
import com.safaricom.pages.contactUS.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.dpHome.backtoSafaricomAppConfirmationPage;
import com.safaricom.pages.dpHome.backtoSafaricomAppFinalConfirmationPage;
import com.safaricom.pages.dpHome.dpHomePage;
import com.safaricom.pages.dpHome.menu_drawer;
import com.safaricom.pages.menu.Menu_AboutUs;
import com.safaricom.pages.menu.Menu_LoginPage;
import com.safaricom.pages.menu.Menu_LogoutConfirmation;
import com.safaricom.pages.menu.Menu_MydataUsage_Conf;
import com.safaricom.pages.menu.Menu_Myprofile;
import com.safaricom.pages.menu.Menu_Netperform;
import com.safaricom.pages.menu.Menu_Netperform_Network;
import com.safaricom.pages.menu.Menu_Netperform_Speedtest;
import com.safaricom.pages.menu.Menu_Netperform_SpeedtestConfirmation;
import com.safaricom.pages.menu.Menu_SetServicePinConf;
import com.safaricom.pages.menu.Menu_SetServicePinFinalConf;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePin;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePinConf;
import com.safaricom.pages.menu.Menu_SettingsSetServicePin;
import com.safaricom.pages.menu.Menu_backtoplatinum;
import com.safaricom.pages.menu.Menu_backtoplatinumfinalConfirmation;
import com.safaricom.pages.menu.Menu_feedback;
import com.safaricom.pages.menu.Menu_feedbackConfirmation;
import com.safaricom.pages.menu.Menu_knowledgebase;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menu_setting;
import com.safaricom.pages.menu.Menu_storelocator;
import com.safaricom.pages.menu.Menu_tellafriend;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.menu.MyaccountHomePage;
import com.safaricom.pages.sfcHome.ContactUSHomePage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.ServicesHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MenuTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject=new MpesaHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public Menudrawer menudrawerObject = new Menudrawer(driver);
	public Menu_setting menusettingObject = new Menu_setting(driver);
	public Menu_SettingsChangeServicePin menusettingchangeservicepinObject=new Menu_SettingsChangeServicePin(driver);
	public Menu_SettingsChangeServicePinConf menusettingchangeservicepinconfObject =new Menu_SettingsChangeServicePinConf(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);
	public MyaccountHomePage myaccounthomePageObject=new MyaccountHomePage(driver);
	public Menu_Netperform menunetperformObject=new Menu_Netperform(driver);
	public Menu_Netperform_Network menunetperformnetworkObject=new Menu_Netperform_Network(driver);
	public Menu_Netperform_Speedtest  menunetperformspeedObject = new Menu_Netperform_Speedtest(driver);
	public Menu_Netperform_SpeedtestConfirmation menunetperformspeedConfirmtaionObject = new Menu_Netperform_SpeedtestConfirmation(driver);
	public Menu_LogoutConfirmation menulogoutconfirmationObject=new Menu_LogoutConfirmation(driver);
	public Menu_LoginPage menuloginPage=new Menu_LoginPage(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	public Menu_backtoplatinum menubacktoplatinumObject=new Menu_backtoplatinum(driver);
	public Menu_backtoplatinumfinalConfirmation menubacktoplatinumfinalConfObject=new Menu_backtoplatinumfinalConfirmation(driver);
	public Menu_AboutUs menuaboutusObject=new Menu_AboutUs(driver);
	public Menu_feedback menufeedbackObject=new Menu_feedback(driver);
	public Menu_feedbackConfirmation menufeedbackconfirmationObject=new Menu_feedbackConfirmation(driver);
	public Menu_tellafriend menutellafriend=new Menu_tellafriend(driver);
	public Menu_storelocator menustorelocatorObject=new Menu_storelocator(driver);
	public Menu_knowledgebase menuknowledgebaseObject=new Menu_knowledgebase(driver);
	public Menu_Myprofile menumyprofileObject=new Menu_Myprofile(driver);
	public Menu_SettingsSetServicePin menusetservicepinObject=new Menu_SettingsSetServicePin(driver);
	public Menu_SetServicePinConf menusetservicepinConfObject=new Menu_SetServicePinConf(driver);
	public Menu_SetServicePinFinalConf menusetservicepinfinalConfObject=new Menu_SetServicePinFinalConf(driver);
	public Menu_MydataUsage_Conf menumydatausageconfObject=new Menu_MydataUsage_Conf(driver);
	public dpHomePage dphomePageObject=new dpHomePage(driver);
	public menu_drawer menuObject=new menu_drawer(driver);
	public backtoSafaricomAppConfirmationPage btsConfObject=new backtoSafaricomAppConfirmationPage(driver);
	public backtoSafaricomAppFinalConfirmationPage btsFinalConfObject=new backtoSafaricomAppFinalConfirmationPage(driver);
	
	/* 
	 * Verify whether the Home Page Option in Menu drawer displays the home Screen.
	 * Title should be "MENU"
	 */
	@Test
	public void MENU_TC_001() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.home_click)).click();
		}
		
		if (sfcHomePageObject.title.getText().equals("MPESA")) {
				
			driver.navigate().back();
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.home_click)).click();
		}
		Thread.sleep(10000);
		String expectedtitle = "MENU";
		String sfctitle = sfcHomePageObject.sfc_title.getText();
		Assert.assertEquals(sfctitle, expectedtitle);
		Assert.assertEquals(true, sfcHomePageObject.checkbalance_click.isDisplayed(),
				"Checkbalance icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.mpesa_click.isDisplayed(), "MPESA icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.tunukiwa_click.isDisplayed(), "Tunukiwa icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.mydataUsage_click.isDisplayed(),
				"My data usage icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.dataandsmsplans_click.isDisplayed(),
				"Data and SMS plans icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.platinumplans_click.isDisplayed(),
				"Platinum plans icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.services_click.isDisplayed(), "Services icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.myaccount_click.isDisplayed(), "My Account icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.contactUs_click.isDisplayed(), "Contact Us icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.sfc_banner.isDisplayed(), "Banner is ot displayed");

	}

	/*
	 * Verify whether Settings expand more with "Set Service Pin" and
	 * "Secure with Pin". If Pin is already set change service pin should be
	 * displayed
	 */
	@Test
	public void MENU_TC_002() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.settings_clicks)).click();

		}
		Thread.sleep(10000);
		Assert.assertEquals(true, menusettingObject.changeservicepin_click.isDisplayed(),
				"Change Service PIN is not displayed");
		Assert.assertEquals(true, menusettingObject.securewithpin_clicks.isDisplayed(),
				"Secure With PIN is ot displayed");

	}

	/*
	 * Verify whether it displays Set Service PIN screen. Should be able to set pin
	 * with the input data.
	 */
	@Test
	public void MENU_TC_003() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.settings_clicks)).click();
		}
		Thread.sleep(10000);
	}

	/*
	 * Verify whether it diplays Change Service PIN screen. Should be able to change
	 * PIN with Old Service PIN,National ID,New Service PIN.
	 */

	@Test
	public void MENU_TC_004() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.settings_clicks)).click();
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(menusettingObject.changeservicepin_click)).click();
		Thread.sleep(10000);
//		String expectedheader="Change Service PIN";
//		String actual_header=menusettingchangeservicepinObject.changeservicepin_header.getText();
//		Assert.assertEquals(actual_header, expectedheader);
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_old_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_national_id)).sendKeys("1234567890");
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_new_pin)).sendKeys("1221");
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_confirm_pin)).sendKeys("1221");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.tv_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinconfObject.tv_ok)).click();
		
	}

	/*
	 * Verify whether it displays confirmation message to set PIN. It Should display
	 * the message for set PIN if PIN is not set.
	 */
	@Test
	public void MENU_TC_005() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.settings_clicks)).click();
		}
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(menusettingObject.securewithpin_clicks)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinObject.ok_btn)).click();
//		String expectedheader="Enter the Service PIN";
//		String actual_header=menusetservicepinConfObject.setservicepin_header.getText();
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinConfObject.et_pin)).sendKeys("1234");
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinConfObject.btn_ok)).click();
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinfinalConfObject.tv_ok)).click();
	
	}

	/*
	 * Verify whether the Title is "MY PROFILE". USERNAME , IMEI NUMBER , PUK NUMBER
	 * should be displayed
	 */
	@Test
	public void MENU_TC_006() throws InterruptedException, IOException {

		
		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		
		}
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.myprofile_click)).click();
		Thread.sleep(5000);
		String expectedtitle="MY PROFILE";
		String mpesa_title=menumyprofileObject.myProfile_title.getText();
		Assert.assertEquals(mpesa_title, expectedtitle);
		Assert.assertEquals(true,menumyprofileObject.img_profile.getText().isEmpty(),"Profile is not displayed");
		Assert.assertEquals(true,menumyprofileObject.tv_name.getText().isEmpty(),"Name is not displayed");
		Assert.assertEquals(true,menumyprofileObject.et_edit_email.getText().isEmpty(),"Email is not diplayed");
		Assert.assertEquals(true,menumyprofileObject.tv_imi_number.getText().isEmpty(),"IMEI Number is not diplayed");
		Assert.assertEquals(true,menumyprofileObject.tv_puk_number.getText().isEmpty(),"PUK Number is not diplayed");
		driver.navigate().back();
	
	}

	/*
	 * Verify whether the Title is "M-PESA". Show balance Card should show the
	 * M-PESA balance. Features like "Send Money,Withdraw Cash,Buy Airtime,Lipa na
	 * M-PESA,Bill Manager, Loans & Savings,My Account,Fuliza M-PESA,M-PESA Global
	 * should be present in MPESA Home Page. M-PESA Statement should display the
	 * MPESA Statement(Full Statement and Mini Statement). Scan QR code should be
	 * present
	 */
	@Test
	public void MENU_TC_007() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(3))).click();
		}
		
		Thread.sleep(10000);
		String expectedtitle="M-PESA";
		String mpesa_title=mpesaHomePageObject.mpesa_title.getText();
		Assert.assertEquals(mpesa_title, expectedtitle);
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_show_balance.isDisplayed(),"Show balance is mot displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_buttonScanQR.isDisplayed(),"Scan QR button is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_statement.isDisplayed(),"MPesa Statement is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.sendMoneyClick.isDisplayed(),"Send Money icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.withdraCashClick.isDisplayed(),"Withdraw Cash icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.buyairtimeClick.isDisplayed(),"Buy airtime icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.lipanampesaClick.isDisplayed(),"Lipanampesa icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.billmanagerClick.isDisplayed(),"Bill Manager icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.loansandsavingsClick.isDisplayed(),"Loans and savings icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.myaccountClick.isDisplayed(),"My Account icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.fulizampesaClick.isDisplayed(),"Fuliza Mpesa icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesaglobalClick.isDisplayed(),"Mpesa Globa icon is no displayed");
		
	}

	/*
	 * Verify whether Services expand more with
	 * "BONGA SERVICES,DATA & SMS PLANS,MY SMS SERVICES,SAMBAZA SERVICES,SKIZA SERVICES,ROAMING SERVICES,OKOA SERVICES & FLEX BUNDLES."
	 */

	@Test
	public void MENU_TC_008() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("M-PESA")) {

			driver.navigate().back();
		
		}
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(4))).click();
		}
		
		Thread.sleep(10000);
		Assert.assertEquals(true, menuservicesObject.bongaservice_click.isDisplayed(),"Bonga Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.datansmsplans_click.isDisplayed(),"Flex icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.mysmsservices_click.isDisplayed(),"Data and SMS Plans icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.sambazaservices_click.isDisplayed(),"My SMS Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.skizaservices_click.isDisplayed(),"Sambaza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.roamingservices_click.isDisplayed(),"Skiza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.okoaservices_click.isDisplayed(),"Roaming Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.flexbundles_click.isDisplayed(),"Okoa Services icon is not displayed");
		
	}

	/*
	 * Verify whether it displays the My account screen with following content
	 * :Data, Bonga, Airtime, SMS, Okoa, Voice cards & tapping on the same should
	 * show the Expiry & balance.Redeemed MMS with count & expiry date.Top Up card
	 * with current time & date with Balance.Options in Top up card - My number/
	 * Other number with input field of Enter 16 digit pin & mobile number field in
	 * case of other number with send button. Buttons - Mpesa Top up & Redeem
	 * Bonga.Contact US card.
	 */
	@Test
	public void MENU_TC_009() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(5))).click();
		}
		
		Thread.sleep(10000);
		String expectedtitle = "MY ACCOUNT";
		String actualtitle = myaccounthomePageObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, myaccounthomePageObject.balance_tab_click.isDisplayed(),"Balance tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isDisplayed(),"PUK tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isDisplayed(),"Top Up tab is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.balance_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.data_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.bonga_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.airtime_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.sms_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.okoa_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.voice_balance_view.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.puk_my_number_click.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_my_number_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.mynum_puk_copy.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.puk_other_number_click.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_other_number_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.puk_et_pin.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.puk_request_send.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_my_number_click.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_my_number_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_et_pin.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.topup_mynum_send.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.topup_other_number_click.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_other_number_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_edt_mobilenumber.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.topup_othernumber_et_pin.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.topup_othernum_send.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.btn_m_pesa_topup_click.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.btn_redeem_bonga_click.isDisplayed());
		Assert.assertEquals(true, myaccounthomePageObject.contactUs_click.isDisplayed());

	}

	/* Verify whether it expands the Options : Network , Speed test */
	@Test
	public void MENU_TC_010() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.netperform_click)).click();
		}
		Thread.sleep(1000);
		Assert.assertEquals(true, menunetperformObject.netperform_network_click.isDisplayed());
		Assert.assertEquals(true, menunetperformObject.netperform_speedtest_clicks.isDisplayed());
	}

	/*
	 * Verify whether it ask for the app usage permisssion, once allowed it should
	 * display the Network screen with tabs mobile, WIFI & their details.
	 */
	@Test
	public void MENU_TC_011() throws InterruptedException, IOException {
		
		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			
		}
		Thread.sleep(1000);
	//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.netperform_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menunetperformObject.netperform_network_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menunetperformnetworkObject.tv_ok)).click();
		driver.navigate().back();
		}
	

	/*
	 * Verify whether is displays the SPEED TEST screen with speedo meter with
	 * details & Start button. Previous speed test reports if any.
	 */
	@Test
	public void MENU_TC_012() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		
		}
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.netperform_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menunetperformObject.netperform_speedtest_clicks)).click();
		String expectedtitle = "SPEED TEST";
		String actualtitle = menunetperformspeedObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(menunetperformspeedObject.testSpeed)).click();
//		wait.until(ExpectedConditions.elementToBeClickable(menunetperformspeedConfirmtaionObject.ok_btn)).click();
		driver.navigate().back();
		driver.navigate().back();
	}

	/*
	 * Verify whether is displays the app usage Confirmation pop, once the access is
	 * allowed it should dispay the Data usage screen with details of the app usage,
	 * date, Total usage.
	 */
	@Test
	public void MENU_TC_013() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
//			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.mydatausage_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(7))).click();
		}
		Thread.sleep(1000);
		String expected_maintitle = "Confirmation";
		String actual_maintitle = menumydatausageconfObject.dlg_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(menumydatausageconfObject.btn_continue)).click();
	}

	/*
	 * Verify whether it displays the knowledge base screen with details. Title
	 * should be "KNOWLEDGE BASE"
	 */
	@Test
	public void MENU_TC_014() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
//			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.knowledgebase)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(8))).click();
		}
		Thread.sleep(1000);
		String expected_maintitle = "KNOWLEDGE BASE";
		String actual_maintitle = menuknowledgebaseObject.knowledgebase_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		
		String expected_subtitle = "Web page not available";
		String actual_subtitle = menuknowledgebaseObject.errorpage_subtitle.getText();
		Assert.assertEquals(false,actual_subtitle.equals(expected_subtitle));
	}

	/*
	 * Verify whether it displays the store locator screen with following content:
	 * Tabs - All shops & Near Me. All shops tab should display the search field
	 * with options centeral, western, Eastern & Nairobi etc. Near me - should ask
	 * for the GPS permission, if granted it should display the near me shop. Title
	 * should be "SHOP LOCATOR"
	 */
	@Test
	public void MENU_TC_015() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.storelocator_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(9))).click();
		}
		Thread.sleep(1000);
		String expected_maintitle = "SHOP LOCATOR";
		String actual_maintitle = menustorelocatorObject.shoplocator_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, menustorelocatorObject.textViewGroupName.isDisplayed(), "Group name is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(menustorelocatorObject.textViewGroupName)).click();
		Assert.assertEquals(true, menustorelocatorObject.txt_name.isDisplayed(), "Name is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_address.isDisplayed(), "Address is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_email.isDisplayed(), "Email is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_weekday.isDisplayed(), "Weekday is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_weekend.isDisplayed(), "Weekend is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(menustorelocatorObject.txt_name)).click();
		driver.navigate().back();
		driver.navigate().back();
		
		
	}

	/*
	 * Verify whether it displays the native options which are available for sharing
	 * eg. Message, Gmail etc.
	 */
	
	@Test
	public void MENU_TC_016() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"Tell a friend\"))");
			elementToClick.click();
			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.tellafriend)).click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(10))).click();
		}
		String expected_maintitle = "Tell a friend";
		String actual_maintitle = menutellafriend.tellafriendtitle.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, menutellafriend.tellafriendicon.isDisplayed(), "ICON is not displayed");
		driver.navigate().back();
	}

	/*
	 * Verify whether it displays the Feedback & rating screen with following
	 * contents:Feedback & Ratting tabs with details.Feedback tab - it should
	 * display the details options, rating option with submit button & social sites
	 * icons. Title should be "FEEDBACK & RATING"
	 */
	
	@Test
	public void MENU_TC_017() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"Feedback & Rating\"))");
			elementToClick.click();
	//		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.feedbacknrating_click)).click();
			//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(11))).click();
		}
		String expected_maintitle = "FEEDBACK & RATING";
		String actual_maintitle = menufeedbackObject.feedback_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_category_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_Category_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_email)).sendKeys("merin.ann@flytxt.com");
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_subject)).sendKeys("Trasaction");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_msg)).sendKeys("Trasaction submit");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_submit)).click();
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackconfirmationObject.ok_btn)).click();
		driver.navigate().back();
		
	}

	/*
	 * Verify whether it display About Us screen with Privacy Policy, Terms &
	 * Conditions, Update History, Service Charter. Title should be "ABOUT US"
	 */
	
	@Test
	public void MENU_TC_018() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"About Us\"))");
			elementToClick.click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.aboutus_click)).click();
	
		}
		String expected_maintitle = "ABOUT US";
		String actual_maintitle = menuaboutusObject.aboutUs_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, menuaboutusObject.img_app_logo.isDisplayed(), "App Logo is not displayed");
		Assert.assertEquals(true, menuaboutusObject.app_name.isDisplayed(),"App name is not displayed");
		Assert.assertEquals(true, menuaboutusObject.app_version.isDisplayed(), "App version is not displayed");
		Assert.assertEquals(true, menuaboutusObject.privacy_policy.isDisplayed(), "Privacy policy is not displayed");
		Assert.assertEquals(true, menuaboutusObject.terms_and_condn.isDisplayed(), "Terms and Condition is not displayed");
		Assert.assertEquals(true, menuaboutusObject.update_history.isDisplayed(), "Update History is not displayed");
		Assert.assertEquals(true, menuaboutusObject.ServiceCharter.isDisplayed(), "Service Charter is not displayed");
		Assert.assertEquals(true, menuaboutusObject.about_us_login_status.isDisplayed(), "About Us Login Status is not displayed");
		Assert.assertEquals(true, menuaboutusObject.copyright.isDisplayed(), "Copyright is not displayed");
		
	}

	/*
	 * Verify whether it displays the Contact us screen with following contents
	 * :Need Support, with social site icons.,New request button.,Contact with us
	 * card with shortcuts of youtube, Blog & Google. Title should be "CONTACT US"
	 */

	@Test
	public void MENU_TC_019() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		if (sfcHomePageObject.title.getText().equals("ABOUT US")) {
driver.navigate().back();
		}
		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"Contact us\"))");
			elementToClick.click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.contactus_click)).click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(13))).click();
		}
	
		String expected_maintitle = "CONTACT US";
		String actual_maintitle = contactUshomePageObject.txt_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, contactUshomePageObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_message_click.isDisplayed(), "Message is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.new_request_click.isDisplayed(), "New Request button is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_youtube_click.isDisplayed(), "Youtube is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_blog_click.isDisplayed(), "Blog is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_google_plus_click.isDisplayed(), "Google Plus is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(contactUshomePageObject.new_request_click)).click();
		
		String expected_subtitle = "REQUEST";
		String actual_subtitle = contactUS_NewRequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.requst_msg)).sendKeys("New Request");
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestFinalConfirmationObject.tv_ok)).click();
	}

	/*
	 * Verify whether it displays a confirmations pop up to confirm whether it
	 * switch back to platinum/blaze. Tap on yes should redirect to platinum/blaze
	 */
	
	@Test
	public void MENU_TC_020() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"Back to Platinum\"))");
			elementToClick.click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.backtoplatinum_click)).click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(14))).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(menubacktoplatinumObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menubacktoplatinumfinalConfObject.okbutton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dphomePageObject.menu_expand)).click();
		MobileElement elementToClick1 = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
				+ "new UiSelector().text(\"BACK TO SAFARICOM APP\"))");
		elementToClick1.click();
		wait.until(ExpectedConditions.elementToBeClickable(btsConfObject.ok_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(btsFinalConfObject.ok_btn)).click();
		
	}

	/*
	 * Verify whether it display the confirmation message, tap on yes should log out
	 * from the app & tap on no should close the pop up.
	 */
	
	@Test
	public void MENU_TC_021() throws InterruptedException, IOException {

		Thread.sleep(1000);
		if (sfcHomePageObject.title.getText().equals("MENU")) {

			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
			MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
					+ "new UiSelector().text(\"Log Out\"))");
			elementToClick.click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.logout_click)).click();
		//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(15))).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(menulogoutconfirmationObject.tv_ok)).click();
		String expectedtitle = "WELCOME TO";
		String actualtitle = menuloginPage.login_welcome_message.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, menuloginPage.tv_generate_pin.isDisplayed());
		System.out.println("Logout Success");
	}

}
