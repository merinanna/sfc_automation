package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;
import com.safaricom.pages.sfcHome.ServicesHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class DatansmsplansTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public DataandsmsplanservicesPage dataandsmsplanservicesPageObject=new DataandsmsplanservicesPage(driver);
	public DSPlanActivateConfirmationPage dsplanActivateComfirmationPageObject=new  DSPlanActivateConfirmationPage(driver);
	public DSPlanActivateFinalConfirmationPage dsplanActivateFinalComfirmationPageObject = new DSPlanActivateFinalConfirmationPage(driver);
	

	/*Verfiy whether 'ACTIVATE' option is present and the title is "DATA & SMS PLANS"*/
	
	@Test
	public void DATASMSPLANS_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.dataandsmsplans_click)).click();
		
		}
		String expected_maintitle = "DATA & SMS PLANS";
		String actual_maintitle = dataandsmsplanservicesPageObject.datasmsplan_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, dataandsmsplanservicesPageObject.activateClick.isDisplayed(), "Activate is not displayed");
		
	}

	/*Verify whether is it possible to Activate Data Plans using "ACTIVATE"*/
	
	@Test
	public void DATASMSPLANS_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.dataandsmsplans_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(dataandsmsplanservicesPageObject.activateClick)).click();
		Assert.assertEquals(true, dsplanActivateComfirmationPageObject.confirmation_message.isDisplayed(),"Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.tv_ok)).click();
		Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.finalconfirmation_message.isDisplayed(),"Final Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.tv_ok)).click();
		
		
	}
	
	@Test
	public void DATASMSPLANS_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);

		
	}
	
}
