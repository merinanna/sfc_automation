package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.LipaNaMpesaPinPage;
import com.safaricom.pages.mpesa.WithdrawCashConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawCashFinalConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawCashErrorPage;
import com.safaricom.pages.mpesa.WithdrawCashSuccessPage;
import com.safaricom.pages.mpesa.WithdrawCashPage;
import com.safaricom.pages.mpesa.WithdrawCashMpesaPinPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class WithdrawCashTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public WithdrawCashPage withdrawCashPageObject = new WithdrawCashPage(driver);
	public WithdrawCashConfirmationPage withdrawCashConfirmationPageObject = new WithdrawCashConfirmationPage(driver);
	public WithdrawCashFinalConfirmationPage withdrawCashFinalConfirmationPageObject = new WithdrawCashFinalConfirmationPage(
			driver);
	public LipaNaMpesaPinPage LipaNaMpesaPinPageObject=new LipaNaMpesaPinPage(driver);

	public WithdrawCashMpesaPinPage WithdrawCashMpesaPinPageObject=new WithdrawCashMpesaPinPage(driver);
	public WithdrawCashSuccessPage WithdrawCashSuccessPageObject=new WithdrawCashSuccessPage(driver);
	public WithdrawCashErrorPage WithdrawCashErrorPageObject=new WithdrawCashErrorPage(driver);
	
		

	/*
	 * Verfy whether the elements "FROM AGENT,FROM ATM" is present in Withdraw Cash
	 * Home Page and Title should be "WITHDRAW CASH"
	 */

	@Test
	public void WITHDRAWCASH_TC_001() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.withdraCashClick)).click();
		String expectedtitle = "WITHDRAW CASH";
		String withdrawcash_title = withdrawCashPageObject.withdrawcash_title.getText();
		System.out.println(withdrawcash_title);
		Assert.assertEquals(withdrawcash_title, expectedtitle);
		
		Assert.assertEquals(true, withdrawCashPageObject.edt_mobilenumber_WFAgent.isDisplayed(),"Enter mobile number field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawCashPageObject.et_amount_WFAgent.isDisplayed(),"Enter amount field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawCashPageObject.WFAgentContinueClick.isDisplayed(),"Continue button for WFAgent is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.L2)).click();
		Assert.assertEquals(true, withdrawCashPageObject.edt_mobilenumber_WFAtm.isDisplayed(),"Enter Mobile number field for WFATM is not displayed ");
		Assert.assertEquals(true, withdrawCashPageObject.WFATMContinueClick.isDisplayed(),"Continue button for WFATM is not displayed");

	}

	/*
	 * Verify whether the "Withdraw From Agent" transaction is getting completed
	 * using valid Agent number and valid Amount
	 */

	@Test
	public void WITHDRAWCASH_TC_002() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

//		if (!(withdrawCashPageObject.edt_mobilenumber_WFAgent.isSelected())) {
//			wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.L2)).click();
			wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.L1)).click();
//
//		}

		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.edt_mobilenumber_WFAgent))
				.sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.et_amount_WFAgent)).sendKeys("200");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.WFAgentContinueClick)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = withdrawCashConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		
	
		
		String mpesa_agent_value = withdrawCashConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		
		String mpesa_amount_value = withdrawCashConfirmationPageObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount value is empty");
		
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashConfirmationPageObject.txt_continue_dilaog))
				.click();
		
		
		
		String expectedtitle="WITHDRAW CASH";
		String withdrawfromagentmpesa_title =WithdrawCashMpesaPinPageObject.withdrawfromagentmpesa_title.getText();
		Assert.assertEquals(withdrawfromagentmpesa_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.ok_btn)).click();
			
//		if(WithdrawCashSuccessPageObject.success_image.isDisplayed())
//		{
//			wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashSuccessPageObject.done_button)).click();
//			WithdrawCashSuccessPageObject.done_button.click();
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}
//			
//		}
//		else {
//			
//			wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashErrorPageObject.done_button)).click();
//			
//		}
		
		
		
//		
//		Thread.sleep(5000);
//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = withdrawCashFinalConfirmationPageObject.final_confirmation_label.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashFinalConfirmationPageObject.finalbutton_click))
//				.click();
//		Assert.assertEquals(actual_label, expected_label);
////		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.backclick)).click();

	}

	/*
	 * Verify whether the "Withdraw From ATM" transaction is getting completed using
	 * valid ATM number
	 */

	@Test
	public void WITHDRAWCASH_TC_003() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.withdraCashClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.L2)).click();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.edt_mobilenumber_WFAtm))
				.sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.WFATMContinueClick)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = withdrawCashConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String txt_business_val_atm = withdrawCashConfirmationPageObject.txt_business_val_atm.getText();
		Assert.assertEquals(false, txt_business_val_atm.isEmpty(),"Atm value is empty");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashConfirmationPageObject.txt_continue_dilaog))
				.click();
		
		
		
		
		String expectedtitle="WITHDRAW CASH";
		String withdrawfromATM_title =WithdrawCashMpesaPinPageObject.withdrawCash_title.getText();
		Assert.assertEquals(withdrawfromATM_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashMpesaPinPageObject.ok_btn)).click();
			
//		if(SendMoneySuccessPageObject.success_image.isDisplayed())
//		{
	//		wait.until(ExpectedConditions.elementToBeClickable(WithdrawCashSuccessPageObject.done_button)).click();
//		WithdrawCashSuccessPageObject.done_button.click();
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}
//			
//		}
//		else {
//			
//			wait.until(ExpectedConditions.elementToBeClickable(SendMoneyErrorPageObject.done_button)).click();
//			
//		}
		
//		
//		Thread.sleep(1000);
//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = withdrawCashFinalConfirmationPageObject.final_confirmation_label.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashFinalConfirmationPageObject.finalbutton_click))
//				.click();
//		Assert.assertEquals(actual_label, expected_label);

	}

}
