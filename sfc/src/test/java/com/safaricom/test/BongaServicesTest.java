package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.checkbalance.DateSelectPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointPage;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.services.AccumulationHistoryConfirmationPage;
import com.safaricom.pages.services.BongaServicesPage;
import com.safaricom.pages.services.RedemptionHistoryConfirmationPage;
import com.safaricom.pages.services.RedemptionHistoryPage;
import com.safaricom.pages.services.TransferBongaPointConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointPage;
import com.safaricom.pages.services.TransferBongaPointServicePINPage;
import com.safaricom.pages.sfcHome.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class BongaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public BongaServicesPage bongaServicePageObject=new BongaServicesPage(driver);
	public RedeemBongaPointPage redeemBongaPointPageObject=new RedeemBongaPointPage(driver);
	public RedeemBongaPointConfirmationPage redeemBongaPointConfirmationPageObject=new RedeemBongaPointConfirmationPage(driver);
	public RedeemBongaPointFinalConfirmationPage redeemBongaPointFinalConfirmationPageObject=new RedeemBongaPointFinalConfirmationPage(driver);
	public TransferBongaPointPage transferBongaPointPageObject=new TransferBongaPointPage(driver);
	public TransferBongaPointConfirmationPage  transferBongaPointConfirmationPageObject=new TransferBongaPointConfirmationPage(driver);
	public TransferBongaPointFinalConfirmationPage transferBongaPointFinalConfirmationPageObject=new TransferBongaPointFinalConfirmationPage(driver);
	public TransferBongaPointServicePINPage transferBongaPointServicePINPageObject=new TransferBongaPointServicePINPage(driver);
	public RedemptionHistoryPage redemptionHistoryPageObject=new RedemptionHistoryPage(driver);
	public DateSelectPage dateSelectPageObject=new DateSelectPage(driver);
	public RedemptionHistoryConfirmationPage redemptionHistoryConfirmationPageObject=new RedemptionHistoryConfirmationPage(driver);
	public AccumulationHistoryConfirmationPage  accumulationHistoryConfirmationPageObject=new AccumulationHistoryConfirmationPage(driver);

	/*Verify whether the icons such as "REDEEM ,TRANSFER ,VIEW REDEMPTION HISTORY and ACCUMULATION HISTORY" are present  and Title should be "BONGA SERVICES".*/
	
	@Test
	public void BONGASERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
		
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			
		}
		
		
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.bongaservicesClick)).click();
		
		String expected_maintitle = "BONGA SERVICES";
		String actual_maintitle = bongaServicePageObject.bongaServices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		
		String expected_title = "BONGA SERVICES";
		String actual_title = bongaServicePageObject.title.getText();
		Assert.assertEquals(actual_title, expected_title);
		
		Assert.assertEquals(true, bongaServicePageObject.redeemClick.isDisplayed(),"Redeem Option  is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.transferClick.isDisplayed(),"Transfer option is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.bongapoints_title.isDisplayed(),"Bongapoint is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.bonga_date.isDisplayed(),"Bonga date is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.bongapoint_value.isDisplayed(),"Bonga Point Value is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.view_redemption_history_click.isDisplayed(),"View Redeemption History is not displayed");
		Assert.assertEquals(true, bongaServicePageObject.accumulation_history_Click.isDisplayed(),"Accumulation history is not displayed");

	}

	//Redeem Bonga Point
	
	public void BONGASERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.bongaservicesClick)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(bongaServicePageObject.redeemClick)).click();
		String expected_maintitle = "Bonga Points";
		String actual_maintitle = redeemBongaPointPageObject.title_bonga_points.getText();
		System.out.println(expected_maintitle + actual_maintitle);
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointConfirmationPageObject.ok_click)).click();
		Thread.sleep(1000);
		String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
		String actual_confirmationmessage = redeemBongaPointFinalConfirmationPageObject.final_confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointFinalConfirmationPageObject.ok_click)).click();
		Thread.sleep(5000);
	}
	
	//Transfer Bonga Point
	
		public void BONGASERVICES_TC_003() throws InterruptedException, IOException {

			Thread.sleep(1000);
			
			
			if (sfcHomePageObject.title.getText().equals("MENU")) {
				wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
				wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.bongaservicesClick)).click();
				
			}
			wait.until(ExpectedConditions.elementToBeClickable(bongaServicePageObject.transferClick)).click();	
			String expected_maintitle = "Transfer Bonga Points";
			String actual_maintitle = transferBongaPointPageObject.title_bonga_points.getText();
			Assert.assertEquals(actual_maintitle, expected_maintitle);
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointPageObject.bonga_mobile_num)).sendKeys("790771777");
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointPageObject.num_of_points)).sendKeys("100");
			driver.navigate().back();
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointPageObject.points_transfer)).click();
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointConfirmationPageObject.ok_button)).click();
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointServicePINPageObject.enter_pin)).sendKeys("1234");
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointServicePINPageObject.btn_ok)).click();
			String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
			String actual_confirmationmessage = transferBongaPointFinalConfirmationPageObject.tv_message.getText();
			Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
			wait.until(ExpectedConditions.elementToBeClickable(transferBongaPointFinalConfirmationPageObject.tv_ok)).click();
			Thread.sleep(5000);
		}
		
		//View Redemption History
		
			public void BONGASERVICES_TC_004() throws InterruptedException, IOException {

				Thread.sleep(1000);
				
				
				if (sfcHomePageObject.title.getText().equals("MENU")) {
					wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
					wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.bongaservicesClick)).click();
					
				}
				
				wait.until(ExpectedConditions.elementToBeClickable(bongaServicePageObject.view_redemption_history_click)).click();
				String expected_maintitle = "Redemption History";
				String actual_maintitle = redemptionHistoryPageObject.txt_dialog_title.getText();
				Assert.assertEquals(actual_maintitle, expected_maintitle);
				wait.until(ExpectedConditions.elementToBeClickable(redemptionHistoryPageObject.text_date1)).click();
				wait.until(ExpectedConditions.elementToBeClickable(dateSelectPageObject.ok_button)).click();
				wait.until(ExpectedConditions.elementToBeClickable(redemptionHistoryPageObject.text_date2)).click();
				wait.until(ExpectedConditions.elementToBeClickable(dateSelectPageObject.ok_button)).click();
				wait.until(ExpectedConditions.elementToBeClickable(redemptionHistoryPageObject.tv_requestforbill)).click();
				Thread.sleep(50000);
				String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
				String actual_confirmationmessage = redemptionHistoryConfirmationPageObject.confirmation_message.getText();
				Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
				wait.until(ExpectedConditions.elementToBeClickable(redemptionHistoryConfirmationPageObject.ok_button)).click();
				Thread.sleep(5000);
						
			}
			
			
			//Accumulation History
			
			public void BONGASERVICES_TC_005() throws InterruptedException, IOException {

				Thread.sleep(1000);
				
				
				if (sfcHomePageObject.title.getText().equals("MENU")) {
					wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
					wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.bongaservicesClick)).click();
					
				}
				wait.until(ExpectedConditions.elementToBeClickable(bongaServicePageObject.accumulation_history_Click)).click();
				Thread.sleep(50000);
				String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
				String actual_confirmationmessage = accumulationHistoryConfirmationPageObject.confirmation_message.getText();
				Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
				wait.until(ExpectedConditions.elementToBeClickable(accumulationHistoryConfirmationPageObject.ok_button)).click();
				Thread.sleep(5000);
				
			}
			
			

}
