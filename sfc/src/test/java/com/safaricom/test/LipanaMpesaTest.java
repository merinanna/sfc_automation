package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.LipaNaMpesaPinPage;
import com.safaricom.pages.mpesa.LipanaMPesaPage;
import com.safaricom.pages.mpesa.LipanaMPesaPageFinalConfirmation;
import com.safaricom.pages.mpesa.LipanaMPesaSendClick;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class LipanaMpesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public LipanaMPesaPage lipanaMPesaPageObject = new LipanaMPesaPage(driver);
	public LipaNaMpesaPinPage LipaNaMpesaPinPageObject=new LipaNaMpesaPinPage(driver);
	public LipanaMPesaSendClick lipanaMPesaSendClickObject = new LipanaMPesaSendClick(driver);
	public LipanaMPesaPageFinalConfirmation lipanaMPesaPageFinalConfirmationObject = new LipanaMPesaPageFinalConfirmation(
			driver);

	/*
	 * Verfy whether the elements "PAY BILL,BUY GOODS AND SERVICES" is present in
	 * Lipa Na M-PESA Home Page and Title should be "LIPA NA M-PESA"
	 */

	@Test
	public void LIPANAMPESA_TC_001() throws InterruptedException, IOException {
		Thread.sleep(5000);

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.lipanampesaClick)).click();
		String expectedtitle = "LIPA NA M-PESA";
		String lipanampesa_title = lipanaMPesaPageObject.lipanampesa_title.getText();
		System.out.println(lipanampesa_title);
		Assert.assertEquals(lipanampesa_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybillClick)).click();
		Assert.assertEquals(true, lipanaMPesaPageObject.paybill_edt_buss_no.isDisplayed(),"Business No field is not displayed");
		Assert.assertEquals(true, lipanaMPesaPageObject.paybill_edt_account_number.isDisplayed(),"Till No field is not diplayed");
		Assert.assertEquals(true, lipanaMPesaPageObject.paybill_edt_bill_amount.isDisplayed(),"Amount field is not displayed");
		Assert.assertEquals(true, lipanaMPesaPageObject.paybill_continue.isDisplayed(),"Continue button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.BuygoodsClick)).click();
		Assert.assertEquals(true, lipanaMPesaPageObject.Buygoods_edt_till_amount.isDisplayed(),"Business No field is not displayed");
		Assert.assertEquals(true, lipanaMPesaPageObject.Buygoods_edt_till_amount.isDisplayed(),"Amount field is not displayed");
		Assert.assertEquals(true, lipanaMPesaPageObject.Buygoods_continue.isDisplayed(),"Continue button is not displayed");

	}

	/*
	 * Verify whether the "Pay Bill" transaction is getting completed with valid
	 * Business Number,Account Number and Amount
	 */

	@Test
	public void LIPANAMPESA_TC_002() throws InterruptedException, IOException {
 
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		Thread.sleep(1000);
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}
		if (!(lipanaMPesaPageObject.paybillClick.isSelected())) {
			
			wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybillClick)).click();

		}
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybill_edt_buss_no)).sendKeys("12345");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybill_edt_account_number)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybill_edt_bill_amount)).sendKeys("200");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.paybill_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title_paybill = "Confirmation";
		String actual_confirmation_title_paybill = lipanaMPesaSendClickObject.lipanampesa_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_paybill, expected_confirmation_title_paybill);
		String txt_business_paybill = lipanaMPesaSendClickObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_paybill.isEmpty(),"Business is empty ");
		String txt_business_accno = lipanaMPesaSendClickObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, txt_business_accno.isEmpty(),"Account Number is empty ");
		String txt_amount = lipanaMPesaSendClickObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, txt_amount.isEmpty(),"Amount is empty ");
		
		Assert.assertEquals(true, lipanaMPesaSendClickObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disbled");
		Assert.assertEquals(true, lipanaMPesaSendClickObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaSendClickObject.txt_continue_dilaog)).click();

		String expectedtitle="LIPA NA M-PESA";
		String sendmoney_title =LipaNaMpesaPinPageObject.lipana_title.getText();
		Assert.assertEquals(sendmoney_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.ok_btn)).click();
		
		
		Thread.sleep(1000);
	
//		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
//		LipaNaMpesaPinPageObject.finalbutton_click.click();

//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = lipanaMPesaPageFinalConfirmationObject.success_message.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageFinalConfirmationObject.btn_cancel)).click();
//		Assert.assertEquals(actual_label, expected_label);
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}
	}

	/*
	 * Verify whether the "Buy Airtime : My Number" transaction is getting completed
	 * with valid Till Number and Amount
	 */

	@Test
	public void LIPANAMPESA_TC_003() throws InterruptedException, IOException {
	
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			
		
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.lipanampesaClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.BuygoodsClick1)).click();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.Buygoods_edt_till_no)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.Buygoods_edt_till_amount)).sendKeys("200");
		driver.navigate().back();
//		PointOption startPoint = PointOption.point(360, 1583);
//		PointOption endPoint = PointOption.point(360, 859);
//		new TouchAction(driver).press(startPoint).moveTo(endPoint).release().perform();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageObject.Buygoods_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title_buygoods = "Confirmation";
		String actual_confirmation_title_buygoods = lipanaMPesaSendClickObject.lipanampesa_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_buygoods, expected_confirmation_title_buygoods);
		String txt_business_buygoods = lipanaMPesaSendClickObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_buygoods.isEmpty(),"Tillno is empty");
		String txt_dialog_account_number_buygoods = lipanaMPesaSendClickObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, txt_dialog_account_number_buygoods.isEmpty(),"Amount_buygoods is empty");
		Assert.assertEquals(true, lipanaMPesaSendClickObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, lipanaMPesaSendClickObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaSendClickObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = lipanaMPesaPageFinalConfirmationObject.success_message.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions.elementToBeClickable(lipanaMPesaPageFinalConfirmationObject.btn_cancel)).click();
//		Assert.assertEquals(actual_label, expected_label);
		String expectedtitle="LIPA NA M-PESA";
		String sendmoney_title =LipaNaMpesaPinPageObject.lipana_title.getText();
		Assert.assertEquals(sendmoney_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.ok_btn)).click();
		
		
		Thread.sleep(1000);
	
//		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
//		LipaNaMpesaPinPageObject.finalbutton_click.click();
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}
	}

}
