package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.contactUS.ContactUS_NewRequest;
import com.safaricom.pages.contactUS.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.mpesa.BuyAirtimeConfirmationPage;
import com.safaricom.pages.mpesa.BuyAirtimeFinalConfirmationPage;
import com.safaricom.pages.mpesa.BuyAirtimePage;
import com.safaricom.pages.myaccount.MyAccount_BalanceHomePage;
import com.safaricom.pages.myaccount.MyAccount_ConfirmationPage;
import com.safaricom.pages.myaccount.MyAccount_PukHomePage;
import com.safaricom.pages.myaccount.MyAccount_TopupHomePage;
import com.safaricom.pages.sfcHome.ContactUSHomePage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.MyaccountHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class MyAccountTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public MyaccountHomePage myaccounthomePageObject=new MyaccountHomePage(driver);
	public MyAccount_BalanceHomePage myaccount_BalanceHomePageObject=new MyAccount_BalanceHomePage(driver);
	public MyAccount_PukHomePage myaccountpukhomepageObject=new MyAccount_PukHomePage(driver);
	public MyAccount_TopupHomePage myaccounttopuphomepageObject=new MyAccount_TopupHomePage(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	public MyAccount_ConfirmationPage myaccount_ConfirmationPageObject=new MyAccount_ConfirmationPage(driver);
	public BuyAirtimePage buyAirtimePageObject = new BuyAirtimePage(driver);
	public BuyAirtimeConfirmationPage buyAirtimeConfirmationPageObject = new BuyAirtimeConfirmationPage(driver);
	public BuyAirtimeFinalConfirmationPage buyAirtimeFinalConfirmationPageObject = new BuyAirtimeFinalConfirmationPage(
			driver);
	

	/*
	 Verify whether "Balances,PUK,Top Up" tabs are present in home page and is clickable. Title should be "MY ACCOUNT"
	 */
	@Test
	public void MYACCOUNT_TC_001() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
		}
		
		String expectedtitle = "MY ACCOUNT";
		String actualtitle = myaccounthomePageObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, myaccounthomePageObject.balance_tab_click.isDisplayed(),"Balance tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isDisplayed(),"PUK tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isDisplayed(),"Top Up tab is displayed");

	}
	
	/*
	Verify whether "DATA,BONGA,AIRTIME,PRE PAID BALANCE,SMS,OKOA,VOICE,REDEEMED MMS" is present when Balances tab is selected and balances are displayed.
	 */
	@Test
	public void MYACCOUNT_TC_002() throws InterruptedException, IOException {
	

		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
		
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.balance_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.balance_tab_click.isSelected(),"Balance tab is selected");
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.data_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.bonga_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.airtime_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.sms_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_balance_view.isDisplayed());
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.data_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.limited_data_amount.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.daily_data_amount.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.data_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.bonga_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.bonga_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.bonga_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.airtime_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.airtime_prepaid_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.airtime_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.sms_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.daily_sms_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.sms_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.okoa_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_airtime_balance.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_data_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.okoa_balance_view)).click();
				
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.voice_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_on_net_talktime.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_off_net_talktime.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.voice_balance_view)).click();
		
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.redeemedmms_txt_amount.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.redeemedmms_txt_date.isDisplayed());
	}
	
	/*
	Verify whether there is option to "Request for PUK" ,  and Contact Us leads to Contact Us home page. Title shoud be "MY ACCOUNT"
	 */
	@Test
	public void MYACCOUNT_TC_003() throws InterruptedException, IOException {
	

		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isSelected(),"PUK tab is selected");
		Assert.assertEquals(true, myaccountpukhomepageObject.labelRequestPUK.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.rb_my_num_puk.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.rb_other_num_puk.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.tv_topup_btn.isDisplayed());	
	}
	
	/*
	Verify whether able to Request PUK for Own Number and option to copy the PUK
	 */
	@Test
	public void MYACCOUNT_TC_004() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		}
		
		if (sfcHomePageObject.title.getText().equals("MY ACCOUNT")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		}
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isSelected(),"puk tab is selected");
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.rb_my_num_puk)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.iv_puk_copy)).click();
	}
	
	/*
	Verify whether able to Request PUK for Other Number with valid mobile number and ID number for other number
	 */
	@Test
	public void MYACCOUNT_TC_005() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		}
		if (sfcHomePageObject.title.getText().equals("MY ACCOUNT")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		}
		
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isSelected(),"PUK tab is selected");
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.rb_other_num_puk)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.tv_topup_btn)).click();

	}
	
	/*
	Verify whether there is option to "TOP UP",   and Contact Us leads to Contact Us home page. Title shoud be "MY ACCOUNT"
	 */
	@Test
	public void MYACCOUNT_TC_006() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isSelected(),"Top up tab is selected");
		String expectedtitle = "MY ACCOUNT";
		String actualtitle = myaccounttopuphomepageObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, myaccounttopuphomepageObject.txt_topup_asat.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.tv_airtime_balance.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.btn_m_pesa_topup_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.btn_redeem_bonga_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.topup_my_number_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.topup_other_number_click.isDisplayed());
		
		
	}
	
	/*
	Verify whether able to TOP UP for Own Number and option to copy the PUK
	 */
	@Test
	public void MYACCOUNT_TC_007() throws InterruptedException, IOException {
	

		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isSelected(),"Top up tab is selected");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_my_number_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_et_pin)).sendKeys("1234567890123456");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_mynum_send)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_ConfirmationPageObject.tv_ok)).click();
		
	}
	
	/*
	Verify whether able to TOP UP for Other Number with valid mobile number and ID number for other number
	 */
	@Test
	public void MYACCOUNT_TC_008() throws InterruptedException, IOException {
	
	

		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isSelected(),"Top up tab is selected");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_other_number_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_othernumber_et_pin)).sendKeys("1234567890123456");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounttopuphomepageObject.topup_othernum_send)).click();
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_ConfirmationPageObject.tv_ok)).click();
		
	}
	
	/*
	 Verify whether the "M-PESA Top Up" is redirected to "Buy Airtime"
	 */
	@Test
	public void MYACCOUNT_TC_009() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		}
		
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isSelected(),"Top up tab is selected");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.btn_m_pesa_topup_click)).click();
		String expectedtitle = "BUY AIRTIME";
		String actualtitle = myaccounttopuphomepageObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		
		

	}
	
	/*
	 Verify whether the "Buy Airtime : My Number" transaction is getting completed with valid Amount
	 */
	@Test
	public void MYACCOUNT_TC_010() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.btn_m_pesa_topup_click)).click();
		}
       		
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyAirtimeConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_agent_value = buyAirtimeConfirmationPageObject.txt_dialog_for.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent Value is empty");
		String mpesa_amount_value = buyAirtimeConfirmationPageObject.tv_mpesa_sendtoamount_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount Value is empty");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyAirtimeFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);

	}
	
	/*
	Verify whether the "Buy Airtime : Other Number" transaction is getting completed with valid Phone number and Amount
	 */
	@Test
	public void MYACCOUNT_TC_011() throws InterruptedException, IOException {
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.btn_m_pesa_topup_click)).click();
		}
       
		if (sfcHomePageObject.title.getText().equals("MY ACCOUNT")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.btn_m_pesa_topup_click)).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyAirtimeConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_agent_value = buyAirtimeConfirmationPageObject.txt_dialog_for.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent Value is empty");
		String mpesa_amount_value = buyAirtimeConfirmationPageObject.tv_mpesa_sendtoamount_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount Value is empty");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyAirtimeFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);

		
	}
	
	/*
	 Verify whether Bonga Point redemption is completed by selecting Redeem Option and Amount
	 */
	@Test
	public void MYACCOUNT_TC_012() throws InterruptedException, IOException {
	
	
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		
		if (sfcHomePageObject.title.getText().equals("M-PESA")) {
			driver.navigate().back();
			
		}
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		}
       
		if (sfcHomePageObject.title.getText().equals("MY ACCOUNT")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.btn_m_pesa_topup_click)).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumberClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_edt_mobilenumber))
				.sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_continue)).click();

		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyAirtimeConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = buyAirtimeConfirmationPageObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile value is empty");
		String mpesa_agent_value = buyAirtimeConfirmationPageObject.txt_dialog_for.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		String mpesa_amount_value = buyAirtimeConfirmationPageObject.tv_mpesa_sendtoamount_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount is empty");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue  button is disabled");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_cancel_dilaog.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyAirtimeFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);
		

	}
	
	/*Verify whether Contact Us is redirected to Contact Us page and user is able to create Request. Titls should be "CONTACT US"*/
	@Test
	public void MYACCOUNT_TC_013() throws InterruptedException, IOException {
	
		Thread.sleep(5000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());
		if (sfcHomePageObject.title.getText().equals("M-PESA")) {
			driver.navigate().back();
			driver.navigate().back();
		}
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.myaccount_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountpukhomepageObject.cv_contact_us)).click();
		String expected_maintitle = "CONTACT US";
		String actual_maintitle = contactUshomePageObject.txt_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, contactUshomePageObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_message_click.isDisplayed(), "Message is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.new_request_click.isDisplayed(), "New Request button is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_youtube_click.isDisplayed(), "Youtube is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_blog_click.isDisplayed(), "Blog is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_google_plus_click.isDisplayed(), "Google Plus is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(contactUshomePageObject.new_request_click)).click();
		
		String expected_subtitle = "REQUEST";
		String actual_subtitle = contactUS_NewRequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.requst_msg)).sendKeys("New Request");
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestFinalConfirmationObject.tv_ok)).click();
		
	}
	
}
