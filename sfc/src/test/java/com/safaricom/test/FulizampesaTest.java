package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.FulizaMPesaConfirmationPage;
import com.safaricom.pages.mpesa.FulizaMPesaPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class FulizampesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public MpesaHomePage mpesahomepageObject = new MpesaHomePage(driver);
	public FulizaMPesaPage fulizapageObject = new FulizaMPesaPage(driver);
	public FulizaMPesaConfirmationPage fulizaconfirmationpageObject = new FulizaMPesaConfirmationPage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	
	/*
	 * Verify whether user is able to Opt in to Fuliza M-Pesa and the title should
	 * be "FULIZA M-PESA"
	 */

	@Test
	public void FULIZAMPESA_TC_001() throws InterruptedException, IOException {
		
		Thread.sleep(10000);
		
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.fulizampesaClick)).click();
		String expectedtitle = "FULIZA M-PESA";
		String fuliza_title = fulizapageObject.fuliza_title.getText();
		wait.until(ExpectedConditions.elementToBeClickable(fulizapageObject.fulizacheckbox_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fulizapageObject.fulizaoptin_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fulizaconfirmationpageObject.fuliza_optin_ok)).click();
		Assert.assertEquals(fuliza_title, expectedtitle);
		Assert.assertEquals(true, fulizapageObject.fulizacheckbox_Click.isDisplayed(),"Checkbox is not displayed");
		Assert.assertEquals(true, fulizapageObject.fulizaoptin_continue.isDisplayed(),"Continue button is not displayed");
	}

}
