package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.mpesa.BillManagerConfirmationPage;
import com.safaricom.pages.mpesa.BillManagerFinalConfirmationPage;
import com.safaricom.pages.mpesa.BillManagerPage;
import com.safaricom.pages.mpesa.LipaNaMpesaPinPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BillManagerTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public BillManagerPage billmanagerPageObject = new BillManagerPage(driver);
	public LipaNaMpesaPinPage LipaNaMpesaPinPageObject=new LipaNaMpesaPinPage(driver);

	public BillManagerConfirmationPage billmanagerConfirmationPageObject = new BillManagerConfirmationPage(driver);
	public BillManagerFinalConfirmationPage billmanagerFinalConfirmationPageObject = new BillManagerFinalConfirmationPage(
			driver);

	/*
	 * Verfy whether the tab "Manage bill" is present in Bill Manager Home Page and
	 * Title should be "MY BILLS"
	 */

	@Test
	public void BILLMANAGER_TC_001() throws InterruptedException, IOException {
		Thread.sleep(5000);

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.billmanagerClick)).click();
		String expectedtitle = "MY BILLS";
		String billmanager_title = billmanagerPageObject.billmanager_title.getText();
		System.out.println(billmanager_title);
		Assert.assertEquals(billmanager_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebilltab_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_imgExpand)).click();
		Thread.sleep(1000);
		Assert.assertEquals(true, billmanagerPageObject.amount_field.isDisplayed(),"Amount Field is not displayed");
		Assert.assertEquals(true, billmanagerPageObject.managebill_paybutton.isDisplayed(),"Pay button is not displayed");

	}

	/* Verify whether Manage bill : pay transaction is complete with valid Amount */

	@Test
	public void BILLMANAGER_TC_002() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.billmanagerClick)).click();
		}

		if (sfcHomePageObject.title.getText().equals("M-PESA")) {
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.billmanagerClick)).click();
		}
		if (!(billmanagerPageObject.amount_field.isDisplayed())) {
			wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_imgExpand)).click();

		}
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.amount_field)).click();
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.amount_field)).sendKeys("200");
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_paybutton)).click();

		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = billmanagerConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String pay_bill_to = billmanagerConfirmationPageObject.pay_bill_to.getText();
		Assert.assertEquals(false, pay_bill_to.isEmpty(),"Pay Bill to field is empty");
		String txtpaybillnum = billmanagerConfirmationPageObject.txtpaybillnum.getText();
		Assert.assertEquals(false, txtpaybillnum.isEmpty(),"Paybill Number field id empty");
//		String tvAccounttName = billmanagerConfirmationPageObject.tvAccounttName.getText();
//		Assert.assertEquals(false, tvAccounttName.isEmpty(),"Account name field is empty");
		String tvAccountNo = billmanagerConfirmationPageObject.tvAccountNo.getText();
		Assert.assertEquals(false, tvAccountNo.isEmpty(),"Account number field is empty");
		String tv_amount = billmanagerConfirmationPageObject.tv_amount.getText();
		Assert.assertEquals(false, tv_amount.isEmpty(),"Amount field is empty");
		String tvTransactionCost = billmanagerConfirmationPageObject.tvTransactionCost.getText();
		Assert.assertEquals(false, tvTransactionCost.isEmpty(),"Transaction cost field is empty");
		Assert.assertEquals(true, billmanagerConfirmationPageObject.buttonNegative.isEnabled());
		Assert.assertEquals(true, billmanagerConfirmationPageObject.buttonPositive.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerConfirmationPageObject.buttonPositive)).click();
		Thread.sleep(1000);
//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = billmanagerFinalConfirmationPageObject.final_confirmation_label.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions.elementToBeClickable(billmanagerFinalConfirmationPageObject.finalbutton_click))
//				.click();
//		Assert.assertEquals(actual_label, expected_label);
//		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.backclick)).click();
		String expectedtitle="BILL MANAGER";
		String sendmoney_title =LipaNaMpesaPinPageObject.lipana_title.getText();
		Assert.assertEquals(sendmoney_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.ok_btn)).click();
		
		
		Thread.sleep(1000);
	
//		wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
//		LipaNaMpesaPinPageObject.finalbutton_click.click();
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}

	}

}
