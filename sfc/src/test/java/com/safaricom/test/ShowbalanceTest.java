package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.ShowbalanceMpesaPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class ShowbalanceTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public ShowbalanceMpesaPage showbalanceMpesaPageObject=new ShowbalanceMpesaPage(driver);
	

	@Test
	public void SHOWBALANCE_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.mpesa_show_balance)).click();
		String expectedtitle="SHOW BALANCE";
		String showbalance_title=showbalanceMpesaPageObject.showbalancempesa_title.getText();
		Assert.assertEquals(showbalance_title, expectedtitle);
		String expectedsubtitle="Check Balance";
		String showbalance_subtitle=showbalanceMpesaPageObject.checkbalance_title.getText();
		Assert.assertEquals(showbalance_subtitle, expectedsubtitle);
		wait.until(ExpectedConditions.elementToBeClickable(showbalanceMpesaPageObject.btn_nine)).click();
		wait.until(ExpectedConditions.elementToBeClickable(showbalanceMpesaPageObject.btn_eight)).click();
		wait.until(ExpectedConditions.elementToBeClickable(showbalanceMpesaPageObject.btn_four)).click();
		wait.until(ExpectedConditions.elementToBeClickable(showbalanceMpesaPageObject.btn_four)).click();
		wait.until(ExpectedConditions.elementToBeClickable(showbalanceMpesaPageObject.btn_ok)).click();
		
	}

}
