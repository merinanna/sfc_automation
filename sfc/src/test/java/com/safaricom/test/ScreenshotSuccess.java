package com.safaricom.test;

import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ScreenshotSuccess {
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	@Test
	static public void screenShotOnError(AppiumDriver<MobileElement> driver) throws InterruptedException, IOException {
			// TODO Auto-generated method stub
			long epoch = System.currentTimeMillis();
		    String ssTimestamp = String.valueOf(epoch);
		    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(scrFile, new File("/home/merin/Desktop/" +"Screenshot"+ ssTimestamp + ".png"));
		}

		
	}

