package com.safaricom.test;

import org.testng.annotations.Test;
import org.testng.Assert;
import java.io.IOException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.LipaNaMpesaPinPage;
import com.safaricom.pages.mpesa.SendMoneyConfirmationPage;
import com.safaricom.pages.mpesa.SendMoneyErrorPage;
import com.safaricom.pages.mpesa.SendMoneyFinalConfirmationPage;
import com.safaricom.pages.mpesa.SendMoneyMpesaPinPage;
import com.safaricom.pages.mpesa.SendMoneyPage;
import com.safaricom.pages.mpesa.SendMoneySuccessPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class SendMoneyTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	public String error;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public SendMoneyPage sendMoneyPageObject = new SendMoneyPage(driver);
	public LipaNaMpesaPinPage LipaNaMpesaPinPageObject=new LipaNaMpesaPinPage(driver);

	public SendMoneyConfirmationPage sendMoneySendClickObject = new SendMoneyConfirmationPage(driver);
	public SendMoneyFinalConfirmationPage sendMoneyFinalConfirmationPageObject = new SendMoneyFinalConfirmationPage(
			driver);
	public SendMoneyMpesaPinPage SendMoneyMpesaPinPageObject =new SendMoneyMpesaPinPage(driver);
	public SendMoneySuccessPage SendMoneySuccessPageObject =new SendMoneySuccessPage(driver);
	public SendMoneyErrorPage SendMoneyErrorPageObject=new SendMoneyErrorPage(driver);
	
	
	/*
	 * Verfy whether the elements "SEND TO ONE,SEND TO OTHER NETWORK" is present in
	 * Send Money Home Page and Title should be "SEND MONEY"
	 */

	@Test
	public void SENDMONEY_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText() );
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.sendMoneyClick)).click();
		String expectedtitle = "SEND MONEY";
		String sendmoney_title = sendMoneyPageObject.SendMoney_title.getText();
		System.out.println(sendmoney_title);
		Assert.assertEquals(sendmoney_title, expectedtitle);
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOnePhoneNumber.isDisplayed(),"Send to One Phone Number is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOnePhoneAmount.isDisplayed(),"Send to One Amount is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOneContinue.isDisplayed(),"Send to One continue button is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherClick.isDisplayed(),"Send ot Other is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherClick)).click();
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherPhoneNumber.isDisplayed(),"Send to Other Phone number is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherPhoneAmount.isDisplayed(),"Send to Other Phone amount is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendToOtherContinue.isDisplayed(),"Send to Other Continue is not displayed");

	}

	/*
	 * Verify whether the "Send To One" transaction is getting completed using valid
	 * Phone number and valid Amount
	 */

	@Test
	public void SENDMONEY_TC_002() throws InterruptedException, IOException {

		
		
		Thread.sleep(1000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText() );
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOneClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneNumber)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneNumber))
				.sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneAmount)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneAmount)).sendKeys("200");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOneContinue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = sendMoneySendClickObject.confirmation_title.getText();
		System.out.println(actual_confirmation_title);
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = sendMoneySendClickObject.tv_mpesa_mobile_value.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile vaue is empty");
		String mpesa_sendto_value = sendMoneySendClickObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_sendto_value.isEmpty(),"Sent value is empty");
		String mpesa_amount_value = sendMoneySendClickObject.tv_mpesa_amount_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount value is empty");
		Assert.assertEquals(true, sendMoneySendClickObject.SendtoOnecancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, sendMoneySendClickObject.SendtoOnecontinue_dilaog.isEnabled(),"Continue button is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneySendClickObject.SendtoOnecontinue_dilaog)).click();
		
		
		String expectedtitle="SEND MONEY";
		String sendmoney_title =SendMoneyMpesaPinPageObject.sendmoney_title.getText();
		Assert.assertEquals(sendmoney_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(SendMoneyMpesaPinPageObject.nine_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(SendMoneyMpesaPinPageObject.eight_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(SendMoneyMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(SendMoneyMpesaPinPageObject.four_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(SendMoneyMpesaPinPageObject.ok_btn)).click();
		
		
//		wait.until(ExpectedConditions.elementToBeClickable(SendMoneySuccessPageObject.done_button)).click();
		
		
//		if (SendMoneySuccessPageObject.success_image.isDisplayed()) {
//			wait.until(ExpectedConditions.elementToBeClickable(SendMoneySuccessPageObject.done_button)).click();
//		SendMoneySuccessPageObject.done_button.click();
		String expectedresult="You have entered the wrong PIN!";
		String actualresult =LipaNaMpesaPinPageObject.finaltext.getText();
		if(actualresult.equals(expectedresult))
		{
			wait.until(ExpectedConditions.elementToBeClickable(LipaNaMpesaPinPageObject.finalbutton_click)).click();
		}
		
//		}
		
//
//		
//		Thread.sleep(5000);
//		String expected_label = "Please wait to enter M-PESA PIN.";
//		String actual_label = sendMoneyFinalConfirmationPageObject.final_confirmation_label.getText();
//		System.out.println(actual_label);
//		wait.until(ExpectedConditions
//				.elementToBeClickable(sendMoneyFinalConfirmationPageObject.SendtoOnefinalbutton_click)).click();
//		Assert.assertEquals(actual_label, expected_label);
		
	}

	/*
	 * Verify whether the "Send To Other Network" transaction is getting completed
	 * using valid Phone number and valid Amount
	 */

	@Test
	public void SENDMONEY_TC_003() throws InterruptedException, IOException {
		
		Thread.sleep(1000);
		System.out.println("Landed Page : " + sfcHomePageObject.title.getText() );
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.sendMoneyClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneNumber)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneNumber))
				.sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneAmount)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneAmount)).sendKeys("200");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendToOtherContinue)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.errormsg_close)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.backclick)).click(); // Click back button
//		String expected_confirmation_title_other = "Confirmation";
//		String actual_confirmation_title_other = sendMoneySendClickObject.confirmation_title.getText();
//		Assert.assertEquals(actual_confirmation_title_other, expected_confirmation_title_other);
//		String mpesa_mobile_value_other = sendMoneySendClickObject.tv_mpesa_mobile_value.getText();
//		Assert.assertEquals(false, mpesa_mobile_value_other.isEmpty(),"Mobile value is empty");
//		String mpesa_agent_value_other = sendMoneySendClickObject.tv_mpesa_agent_value.getText();
//		Assert.assertEquals(false, mpesa_agent_value_other.isEmpty(),"Agent Value is empty");
//		String mpesa_amount_value_other = sendMoneySendClickObject.tv_mpesa_amount_value.getText();
//		Assert.assertEquals(false, mpesa_amount_value_other.isEmpty(),"Amount value is empty");
//		Assert.assertEquals(true, sendMoneySendClickObject.SendtoOnecancel_dilaog.isEnabled(),"Cancel  button is disabled");
//		Assert.assertEquals(true, sendMoneySendClickObject.SendtoOnecontinue_dilaog.isEnabled(),"Continue button is disabled");
//		wait.until(ExpectedConditions.elementToBeClickable(sendMoneySendClickObject.SendtoOnecontinue_dilaog)).click();
//		
//		Thread.sleep(5000);
//		String expected_label_other = "Please wait to enter M-PESA PIN.";
//		String actual_label_other = sendMoneyFinalConfirmationPageObject.final_confirmation_label.getText();
//		Assert.assertEquals(actual_label_other, expected_label_other);

	}

}
