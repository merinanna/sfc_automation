package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class SFCHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);

	/*
	 * Verify whether the icons such as
	 * "CHECKBALANCE,M-PESA,TUNUKIWA,MY DATA USAGE,DATA AND SMS PLANS,PLATINUM PLANS,SERVICES,MY ACCOUNT AND CONTACT US "
	 * and the "BANNER" is displayed in SFC Home Page and Title should be "MENU"
	 */
	@Test
	public void SFC_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		Assert.assertEquals(true, sfcHomePageObject.checkbalance_click.isDisplayed(),"Checkbalance icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.mpesa_click.isDisplayed(),"MPESA icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.tunukiwa_click.isDisplayed(),"Tunukiwa icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.mydataUsage_click.isDisplayed(),"My data usage icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.dataandsmsplans_click.isDisplayed(),"Data and SMS plans icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.platinumplans_click.isDisplayed(),"Platinum plans icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.services_click.isDisplayed(),"Services icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.myaccount_click.isDisplayed(),"My Account icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.contactUs_click.isDisplayed(),"Contact Us icon is not displayed");
		Assert.assertEquals(true, sfcHomePageObject.sfc_banner.isDisplayed(),"Banner is not displayed");
	}

}
