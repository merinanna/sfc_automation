package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.checkbalance.DSPlanActivateConfirmationPage;
import com.safaricom.pages.checkbalance.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.checkbalance.DataandsmsplanservicesPage;
import com.safaricom.pages.checkbalance.DateSelectPage;
import com.safaricom.pages.checkbalance.FullStatementPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.checkbalance.RedeemBongaPointPage;
import com.safaricom.pages.checkbalance.TransactionCodePage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.sfcHome.CheckbalanceHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class CheckbalanceTest{
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject=new sfcHomePage(driver);
	public CheckbalanceHomePage checkbalanceHomepageObject=new CheckbalanceHomePage();
	public LoginFailurePage loginfailureObject=new LoginFailurePage(driver);
	public RedeemBongaPointPage redeemBongaPointPageObject=new RedeemBongaPointPage(driver);
	public RedeemBongaPointConfirmationPage redeemBongaPointConfirmationPageObject=new RedeemBongaPointConfirmationPage(driver);
	public RedeemBongaPointFinalConfirmationPage redeemBongaPointFinalConfirmationPageObject=new RedeemBongaPointFinalConfirmationPage(driver);
	public DataandsmsplanservicesPage dataandsmsplanservicesPageObject=new DataandsmsplanservicesPage(driver);
	public DSPlanActivateConfirmationPage dsplanActivateComfirmationPageObject=new  DSPlanActivateConfirmationPage(driver);
	public DSPlanActivateFinalConfirmationPage dsplanActivateFinalComfirmationPageObject = new DSPlanActivateFinalConfirmationPage(driver);
	public FullStatementPage fullstatementPageObject=new FullStatementPage(driver);
	public DateSelectPage dateselectpageObject=new DateSelectPage(driver);
	public TransactionCodePage transactionPageObject=new TransactionCodePage(driver);
	
	/*Verify whether Account Balance is shown with corresponding date ,Data Balance and SMS Balance is shown, Option available for Redeem Bonga Points,Buy Data,Full Statement and Mini statement. Title should be "CHECK BALANCE"*/

	@Test
	public void CHECKBALANCE_TC_001() throws InterruptedException, IOException {

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.checkbalance_click)).click();
			
		}
		sfcHomePageObject.checkbalance_click.click();
		String expectedtitle="CHECK BALANCE";
		String checkbalancetitle=checkbalanceHomepageObject.checkbalancetitle.getText();
		Assert.assertEquals(checkbalancetitle, expectedtitle);
		Assert.assertEquals(true, checkbalanceHomepageObject.btn_redeem_bonga_click.isDisplayed());
		Assert.assertEquals(true, checkbalanceHomepageObject.btn_buy_data_click.isDisplayed());
		Assert.assertEquals(true, checkbalanceHomepageObject.buttonFullStatement_click.isDisplayed());
		Assert.assertEquals(true, checkbalanceHomepageObject.buttonMiniStatement_click.isDisplayed());
			}
	
	/*Verify whether Bonga Point redemption is completed by selecting Redeem Option and Amount*/
	
	@Test
	public void CHECKBALANCE_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.checkbalance_click)).click();
			
		}
		wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.btn_redeem_bonga_click)).click();
		String expected_maintitle = "Bonga Points";
		String actual_maintitle = redeemBongaPointPageObject.title_bonga_points.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointConfirmationPageObject.ok_click)).click();
		String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
		String actual_confirmationmessage = redeemBongaPointFinalConfirmationPageObject.final_confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointFinalConfirmationPageObject.ok_click)).click();
		
	}
	
	/*Verify whether is it possible to Activate Data and SMS Plans using "ACTIVATE"*/
	@Test
	public void CHECKBALANCE_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.checkbalance_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.btn_buy_data_click)).click();
			
		}
		if (sfcHomePageObject.title.getText().equals("CHECK BALANCE")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.btn_buy_data_click)).click();
		
		}
		
		String expected_maintitle = "DATA & SMS PLANS";
		String actual_maintitle = dataandsmsplanservicesPageObject.datasmsplan_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, dataandsmsplanservicesPageObject.activateClick.isDisplayed(), "Activate is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dataandsmsplanservicesPageObject.activateClick)).click();
		Assert.assertEquals(true, dsplanActivateComfirmationPageObject.confirmation_message.isDisplayed(),"Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.tv_ok)).click();
		Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.finalconfirmation_message.isDisplayed(),"Final Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.tv_ok)).click();
		
	}
	
	/*Verify whether Full Statement is generated with valid email address , start date and end date*/
	@Test
	public void CHECKBALANCE_TC_004() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.checkbalance_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.buttonFullStatement_click)).click();
			
		}
		if (sfcHomePageObject.title.getText().equals("CHECK BALANCE")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.buttonFullStatement_click)).click();
		
		}
		
		String expected_maintitle = "Prepaid Usage Statement";
		String actual_maintitle = fullstatementPageObject.ministatement_labeltitle.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, fullstatementPageObject.inputEmail.isDisplayed(), "Email field is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(fullstatementPageObject.inputEmail)).sendKeys("merin.ann@flytxt.com");
		wait.until(ExpectedConditions.elementToBeClickable(fullstatementPageObject.labelFromDate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dateselectpageObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fullstatementPageObject.labelToDate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dateselectpageObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fullstatementPageObject.request_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(transactionPageObject.inputTransactionCode)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(transactionPageObject.submit_btn)).click();
		
	}
	
	/*Verify whether Mini Statement is generated */
	@Test
	public void CHECKBALANCE_TC_005() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.checkbalance_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.buttonMiniStatement_click)).click();
			
		}
		if (sfcHomePageObject.title.getText().equals("CHECK BALANCE")) {
			
			wait.until(ExpectedConditions.elementToBeClickable(checkbalanceHomepageObject.buttonMiniStatement_click)).click();
		
		}
	}
	
	}	


