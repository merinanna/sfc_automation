package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MpesaHomeTest{
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject=new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject=new MpesaHomePage(driver);
	
	@Test
	public void MPESA_TC_001() throws InterruptedException, IOException {

		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		String expectedtitle="M-PESA";
		String mpesa_title=mpesaHomePageObject.mpesa_title.getText();
		Assert.assertEquals(mpesa_title, expectedtitle);
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_show_balance.isDisplayed(),"Show balance is mot displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_buttonScanQR.isDisplayed(),"Scan QR button is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesa_statement.isDisplayed(),"MPesa Statement is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.sendMoneyClick.isDisplayed(),"Send Money icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.withdraCashClick.isDisplayed(),"Withdraw Cash icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.buyairtimeClick.isDisplayed(),"Buy airtime icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.lipanampesaClick.isDisplayed(),"Lipanampesa icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.billmanagerClick.isDisplayed(),"Bill Manager icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.loansandsavingsClick.isDisplayed(),"Loans and savings icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.myaccountClick.isDisplayed(),"My Account icon is not diplayed");
		Assert.assertEquals(true,mpesaHomePageObject.fulizampesaClick.isDisplayed(),"Fuliza Mpesa icon is not displayed");
		Assert.assertEquals(true,mpesaHomePageObject.mpesaglobalClick.isDisplayed(),"Mpesa Globa icon is no displayed");
		
	}
	}	


