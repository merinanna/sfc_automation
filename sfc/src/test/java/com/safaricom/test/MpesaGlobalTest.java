package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.MPesaGlobalPage;
import com.safaricom.pages.sfcHome.MpesaHomePage;
import com.safaricom.pages.sfcHome.sfcHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

@Test
public class MpesaGlobalTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public MpesaHomePage mpesahomepageObject = new MpesaHomePage(driver);
	public MPesaGlobalPage mpesaGlobalObject = new MPesaGlobalPage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	/*
	 * Verify whether user is able to Opt in to M-Pesa Global and the title should
	 * be "M-PESA GLOBAL"
	 */

	@Test
	public void MPESAGLOBAL_TC_001() throws InterruptedException, IOException {

		Thread.sleep(10000);
		
		if(sfcHomePageObject.title.getText().equals("MENU"))
		{
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
			
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.mpesaglobalClick)).click();
		String expectedtitle = "M-PESA GLOBAL";
		String mpesaglobal_title = mpesaGlobalObject.mpesaglobal_title.getText();
		PointOption startPoint = PointOption.point(786, 1254);
		PointOption endPoint = PointOption.point(759, 801);
		new TouchAction(driver).press(startPoint).moveTo(endPoint).release().perform();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.region)).sendKeys("India");
		wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.termsandconditioncheckbox_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.mpesaglobaloptin_continue)).click();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true, mpesaGlobalObject.region.isDisplayed(),"MpesaGlobalRegion not displayed");
		Assert.assertEquals(true, mpesaGlobalObject.termsandconditioncheckbox_Click.isDisplayed(),"Terms and Conditions checkbox is not displayed");
		Assert.assertEquals(true, mpesaGlobalObject.mpesaglobaloptin_continue.isDisplayed(),"MpesaGlobal Optin is not displayed" );

	}

}
