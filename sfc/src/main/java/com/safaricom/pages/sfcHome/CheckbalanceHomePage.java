package com.safaricom.pages.sfcHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CheckbalanceHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public CheckbalanceHomePage() {
	  
  }
  public CheckbalanceHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement checkbalancetitle;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem_bonga")
  public AndroidElement btn_redeem_bonga_click;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_buy_data")
  public AndroidElement btn_buy_data_click ;

  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonFullStatement")
  public AndroidElement buttonFullStatement_click;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMiniStatement")
  public AndroidElement buttonMiniStatement_click ;
  
 
}
