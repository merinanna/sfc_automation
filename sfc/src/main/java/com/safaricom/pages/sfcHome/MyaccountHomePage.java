package com.safaricom.pages.sfcHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyaccountHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public MyaccountHomePage() {
	  
  }
  public MyaccountHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement txt_title;
  
  // Balance
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Balances']")
  public AndroidElement balance_tab_click;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data']")
  public AndroidElement data_balance_view ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga']")
  public AndroidElement bonga_balance_view;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Airtime']")
  public AndroidElement airtime_balance_view ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SMS']")
  public AndroidElement sms_balance_view;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa']")
  public AndroidElement okoa_balance_view;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Voice']")
  public AndroidElement voice_balance_view ;
  

  
  //PUK
  @AndroidFindBy(xpath="//android.widget.TextView[@text='PUK']")
  public AndroidElement puk_click ;
   
  //PUK MY NUMBER
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Number']")
  public AndroidElement puk_my_number_click ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_puk_copy")
  public AndroidElement mynum_puk_copy ;
  
  //PUK OTHER NUMBER
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Other Number']")
  public AndroidElement puk_other_number_click ;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement puk_et_pin ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement puk_request_send ;
  
  
  
  //Top Up
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Top Up']")
  public AndroidElement topup_tab_click ;
    
  //My Number 
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='My Number']")
  public AndroidElement topup_my_number_click ;

  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement topup_et_pin;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement topup_mynum_send ;
  
  //Other Number 
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Other Number']")
  public AndroidElement topup_other_number_click;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
  public AndroidElement topup_edt_mobilenumber;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement topup_othernumber_et_pin;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement topup_othernum_send ;
  
  //M-Pesa TopUp
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_m_pesa_topup")
  public AndroidElement btn_m_pesa_topup_click ;
  
  //Redeem Bonga
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem_bonga")
  public AndroidElement btn_redeem_bonga_click ;
  
  //ContactUS
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Contact us']")
  public AndroidElement contactUs_click ;
}
