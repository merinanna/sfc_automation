package com.safaricom.pages.sfcHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MydataUsageConfirmation {
	public AndroidDriver<MobileElement> driver;
  
  public MydataUsageConfirmation() {
	  
  }
  public MydataUsageConfirmation(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
  private AndroidElement MydataUsage_btn_continue;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  private AndroidElement MydataUsage_btn_cancel;
 }
