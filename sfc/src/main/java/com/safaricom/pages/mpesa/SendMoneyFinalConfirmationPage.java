package com.safaricom.pages.mpesa;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendMoneyFinalConfirmationPage {
 
	public AndroidDriver<MobileElement>  driver;
	public SendMoneyFinalConfirmationPage()
	{
		
	}
	
	public SendMoneyFinalConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//<-------------------------Send to One-------------------------------->
	
	
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement final_confirmation_label;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement SendtoOnefinalbutton_click;
	
}
