package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountMPESA1Tap {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountMPESA1Tap()
	{
		
	}
	
	public MPesaMyaccountMPESA1Tap(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement mpesa1tap_title; //M-PESA 1 TAPR
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/bind_btn_get_card")
	public AndroidElement bind_btn_get_card;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/add_shortcut_button")
	public AndroidElement add_shortcut_button;
	
	
	
	}
