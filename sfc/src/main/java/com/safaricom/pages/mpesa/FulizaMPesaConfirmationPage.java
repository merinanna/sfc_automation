package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FulizaMPesaConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public FulizaMPesaConfirmationPage()
	{
		
	}
	
	public FulizaMPesaConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	// Fuliza Optin
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement fuliza_optin_ok;
	
			
	}
