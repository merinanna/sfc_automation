package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Locksavingsaccount_SaveConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public Locksavingsaccount_SaveConfirmationPage()
	{
		
	}
	
	public Locksavingsaccount_SaveConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  //Savings 
	  
	  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView")
	  public AndroidElement save_txt_dialog_title;         //Save
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	  public AndroidElement save_txt_dialog_amount;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	  public AndroidElement save_tv_mpesa_sendto_value;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_mobile_value")
	  public AndroidElement save_tv_mpesa_mobile_value;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_agent_value")
	  public AndroidElement save_tv_mpesa_agent_value;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	  public AndroidElement save_txt_cancel_dilaog;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	  public AndroidElement save_txt_continue_dilaog;
	 
	  
	
	
	}
