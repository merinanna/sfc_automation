package com.safaricom.pages.mpesa;


import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ShowbalanceMpesaPage {
 
	public AndroidDriver<MobileElement> driver;
	public ShowbalanceMpesaPage()
	{
		
	}
	
	public ShowbalanceMpesaPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 // SHOW BALANCE MPESA PAGE
	
	  @FindBy(how = How.CLASS_NAME,using="android.widget.TextView")
      public  List<MobileElement> L1;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement showbalancempesa_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pinTV")
	  public AndroidElement checkbalance_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/input_pin")
	  public AndroidElement inputpin_field;
	
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_1")
	  public AndroidElement btn_one;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_2")
	  public AndroidElement btn_two;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_3")
	  public AndroidElement btn_three;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_4")
	  public AndroidElement btn_four;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_5")
	  public AndroidElement btn_five;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_6")
	  public AndroidElement btn_six;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_7")
	  public AndroidElement btn_seven;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_8")
	  public AndroidElement btn_eight;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_9")
	  public AndroidElement btn_nine;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_0")
	  public AndroidElement btn_zero;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_ok")
	  public AndroidElement btn_ok;
	}
