package com.safaricom.pages.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawCashMpesaPinPage {
 
	public AndroidDriver<MobileElement>  driver;
	public WithdrawCashMpesaPinPage()
	{
		
	}
	
	public WithdrawCashMpesaPinPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 @FindBy(how = How.CLASS_NAME,using="android.widget.TextView")
     public  List<MobileElement> L1;
	 
	 @FindBy(how = How.CLASS_NAME,using="android.widget.ImageButton")
     public  List<MobileElement> L2;
	 
	  
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='WITHDRAW CASH']")
	  public AndroidElement withdrawCash_title ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	  public AndroidElement one_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[2]")
	  public AndroidElement two_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView[3]")
	  public AndroidElement three_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]")
	  public AndroidElement four_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[2]")
	  public AndroidElement five_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[3]")
	  public AndroidElement six_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[1]")
	  public AndroidElement seven_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[2]")
	  public AndroidElement eight_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.TextView[3]")
	  public AndroidElement nine_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView[1]")
	  public AndroidElement zero_btn ;
	 
	 @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.TextView[2]")
	  public AndroidElement ok_btn ;
	 
	  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.TextView")
	  public AndroidElement withdrawfromagentmpesa_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pinTV")
	  public AndroidElement withdrawfromagent_subtitle;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/input_pin")
	  public AndroidElement inputpin_field;
	
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_1")
	  public AndroidElement btn_one;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_2")
	  public AndroidElement btn_two;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_3")
	  public AndroidElement btn_three;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_4")
	  public AndroidElement btn_four;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_5")
	  public AndroidElement btn_five;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_6")
	  public AndroidElement btn_six;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_7")
	  public AndroidElement btn_seven;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_8")
	  public AndroidElement btn_eight;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_9")
	  public AndroidElement btn_nine;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_0")
	  public AndroidElement btn_zero;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_clear")
	  public AndroidElement btn_clear;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_ok")
	  public AndroidElement btn_ok;
	
}
