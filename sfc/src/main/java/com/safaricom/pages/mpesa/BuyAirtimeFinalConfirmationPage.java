package com.safaricom.pages.mpesa;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyAirtimeFinalConfirmationPage {
 
	public AndroidDriver<MobileElement>  driver;
	public BuyAirtimeFinalConfirmationPage()
	{
		
	}
	
	public BuyAirtimeFinalConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	

	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement final_confirmation_label;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement finalbutton_click;
	
}
