package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Locksavinsaccout_SaveFinalConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public Locksavinsaccout_SaveFinalConfirmationPage()
	{
		
	}
	
	public Locksavinsaccout_SaveFinalConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	  public AndroidElement labelMessage;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/button")
	  public AndroidElement ok_button;
	  	  
	
	
	}
