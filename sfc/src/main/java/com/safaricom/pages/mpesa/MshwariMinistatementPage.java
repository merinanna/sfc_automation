package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MshwariMinistatementPage {
 
	public AndroidDriver<MobileElement> driver;
	public MshwariMinistatementPage()
	{
		
	}
	
	public MshwariMinistatementPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//M-Shwari Home Page
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Send to M-Shwari']")
	  public AndroidElement sendtomshwari_click;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Withdraw from\n" + 
	  		"M-Shwari']")
	  public AndroidElement withdrawfrommshwari_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Lock Savings Account']")
	  public AndroidElement locksavingsaccount_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Loan']")
	  public AndroidElement loan_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Mini Statement']")
	  public AndroidElement ministatement_click;
	  
	  
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement mshwari_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textSavings")
	  public AndroidElement textSavings;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanAmt")
	  public AndroidElement textLoanAmt;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textDueDate")
	  public AndroidElement textDueDate;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanLimit")
	  public AndroidElement textLoanLimit;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textTargetAmt")
	  public AndroidElement textTargetAmt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textMaturityDate")
	  public AndroidElement textMaturityDate;
	
	
	}
