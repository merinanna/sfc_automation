package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FulizaMPesaPage {
 
	public AndroidDriver<MobileElement> driver;
	public FulizaMPesaPage()
	{
		
	}
	
	public FulizaMPesaPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	// Fuliza Optin
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement fuliza_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/checkBoxTerms")
	public AndroidElement fulizacheckbox_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement fulizaoptin_continue;
	
	
	
	
	
	}
