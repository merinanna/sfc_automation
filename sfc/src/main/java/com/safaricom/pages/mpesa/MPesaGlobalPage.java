package com.safaricom.pages.mpesa;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaGlobalPage {

	public AndroidDriver<MobileElement> driver;

	public MPesaGlobalPage() {

	}

	public MPesaGlobalPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// MPESA Global Optin

	@AndroidFindBy(id = "com.selfcare.safaricom:id/txt_title")
	public AndroidElement mpesaglobal_title; // M-PESA GLOBAL

	@AndroidFindBy(id = "com.selfcare.safaricom:id/location")
	public AndroidElement region;

	@AndroidFindBy(id = "com.selfcare.safaricom:id/checkBox_terms_cond")
	public AndroidElement termsandconditioncheckbox_Click;

	@AndroidFindBy(id = "com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement mpesaglobaloptin_continue;

}
