package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Ministatement_DepositHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public Ministatement_DepositHomePage()
	{
		
	}
	
	public Ministatement_DepositHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	  // Deposit Ministatement Home Page

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tvStmtHeader")
	  public AndroidElement depositministatement_header; // Deposit Mini Statement
	  
	  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView")
	  public AndroidElement succss_message;   
	  
	  
	  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.TextView")
	  public AndroidElement btn_cancel; 
	  
	}
