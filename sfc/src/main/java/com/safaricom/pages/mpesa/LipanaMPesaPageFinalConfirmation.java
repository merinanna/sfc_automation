package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LipanaMPesaPageFinalConfirmation {
 
	public AndroidDriver<MobileElement> driver;
	public LipanaMPesaPageFinalConfirmation()
	{
		
	}
	
	public LipanaMPesaPageFinalConfirmation(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//FinalConfirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
	public AndroidElement success_message;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
	public AndroidElement btn_cancel;
	

	
	
	
	}
