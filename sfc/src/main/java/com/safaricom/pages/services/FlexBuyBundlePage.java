package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FlexBuyBundlePage {
	public AndroidDriver<MobileElement> driver;
  
  public FlexBuyBundlePage() {
	  
  }
  public FlexBuyBundlePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/dlg_title")
  public AndroidElement buybundle_title;

  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='Buy Once']")
  public AndroidElement buyOnce_click;
  
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='Auto Renew']")
  public AndroidElement autoRenew_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
  public AndroidElement btn_continue;

  
}
