package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FlexBuyBundle_FinalConfirmationPage {
	public AndroidDriver<MobileElement> driver;
  
  public FlexBuyBundle_FinalConfirmationPage() {
	  
  }
  public FlexBuyBundle_FinalConfirmationPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
  public AndroidElement succss_message;

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement ok_btn;

  
}
