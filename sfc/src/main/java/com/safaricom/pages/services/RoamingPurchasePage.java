package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RoamingPurchasePage {
	public AndroidDriver<MobileElement> driver;
  
  public RoamingPurchasePage() {
	  
  }
  public RoamingPurchasePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Roaming Bundles']")
  public AndroidElement roamingbundle_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_roamign_service_title")
  public AndroidElement tv_roamign_service_title;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_amount_roaming")
  public AndroidElement tv_amount_roaming;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_date_airtime")
  public AndroidElement tv_date_airtime;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_roaming_purchase")
  public AndroidElement btn_roaming_purchase ;

}
