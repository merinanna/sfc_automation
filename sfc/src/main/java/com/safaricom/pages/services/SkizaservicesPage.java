package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SkizaservicesPage {
	public AndroidDriver<MobileElement> driver;
  
  public SkizaservicesPage() {
	  
  }
  public SkizaservicesPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement skizaservices_title;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_artist")
  public AndroidElement rbartist_click;
      
  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_Song")
  public AndroidElement rbsong_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/entr_artist_song")
  public AndroidElement artist_song ;

  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tune_search_btn")
  public AndroidElement search_btn_Click;
  
}
