package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RoamingPurchaseFinalConfirmationPage {
	public AndroidDriver<MobileElement> driver;
  
  public RoamingPurchaseFinalConfirmationPage() {
	  
  }
  public RoamingPurchaseFinalConfirmationPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
  public AndroidElement tv_message;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
  public AndroidElement tv_cancel;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
  public AndroidElement tv_ok;

}
