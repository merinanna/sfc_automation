package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MysmsservicesPage {
	public AndroidDriver<MobileElement> driver;
  
  public MysmsservicesPage() {
	  
  }
  public MysmsservicesPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement mysmsservices_title;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_activate")
  public AndroidElement deactivateClick ;
 
}
