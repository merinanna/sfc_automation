package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DateSelectPage {
 public AndroidDriver<MobileElement> driver;
  
  public DateSelectPage() {
	  
  }
  public DateSelectPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="android:id/date_picker_header_year")
  public AndroidElement date_picker_header_year;
  
  @AndroidFindBy(id="android:id/date_picker_header_date")
  public AndroidElement date_picker_header_date;
    
  @AndroidFindBy(xpath="//android.widget.Button[@text='OK']")
  public AndroidElement ok_button ;
    
  @AndroidFindBy(xpath="//android.widget.Button[@text='Cancel']")
  public AndroidElement cancel_button ;
  
  }
