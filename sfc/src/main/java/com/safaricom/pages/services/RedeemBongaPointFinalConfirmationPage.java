package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RedeemBongaPointFinalConfirmationPage {
 public AndroidDriver<MobileElement> driver;
  
  public RedeemBongaPointFinalConfirmationPage() {
	  
  }
  public RedeemBongaPointFinalConfirmationPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
  public AndroidElement final_confirmation_message;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
  public AndroidElement ok_click ;
  
  
  }
