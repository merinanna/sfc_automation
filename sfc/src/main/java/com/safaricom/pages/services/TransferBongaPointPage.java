package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TransferBongaPointPage {
 public AndroidDriver<MobileElement> driver;
  
  public TransferBongaPointPage() {
	  
  }
  public TransferBongaPointPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_bonga_mobile_num")
  public AndroidElement bonga_mobile_num;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_num_of_points")
  public AndroidElement num_of_points ;

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_points_transfer")
  public AndroidElement points_transfer;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_title_bonga_points")
  public AndroidElement title_bonga_points ;
  
  }
