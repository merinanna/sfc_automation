package com.safaricom.pages.dpHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class backtoSafaricomAppConfirmationPage {
	public AndroidDriver<MobileElement> driver;
  
  public backtoSafaricomAppConfirmationPage() {
	  
  }
  public backtoSafaricomAppConfirmationPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
  public AndroidElement conf_message ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
  public AndroidElement cancel_btn ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
  public AndroidElement ok_btn ;
}
