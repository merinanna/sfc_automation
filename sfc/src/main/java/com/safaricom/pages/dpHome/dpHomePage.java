package com.safaricom.pages.dpHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class dpHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public dpHomePage() {
	  
  }
  public dpHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_menu_expand")
  public AndroidElement menu_expand;
  
}
