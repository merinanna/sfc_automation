package com.safaricom.pages.dpHome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class menu_drawer {
	public AndroidDriver<MobileElement> driver;
  
  public menu_drawer() {
	  
  }
  public menu_drawer(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BACK TO SAFARICOM APP']")
  public AndroidElement backtosafaricom_click;
      
}
