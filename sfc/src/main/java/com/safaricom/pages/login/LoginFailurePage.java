package com.safaricom.pages.login;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginFailurePage {
	
	public AndroidDriver<MobileElement> driver;
	public LoginFailurePage() {
		
	}
	
	public LoginFailurePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	public AndroidElement otp_msg;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement ok_btn_click;
	
}
