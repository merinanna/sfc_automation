package com.safaricom.pages.login;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LocationPermissionPage {

	public AndroidDriver<MobileElement> driver;

	public LocationPermissionPage() {

	}

	public LocationPermissionPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	public MobileElement locationpermission;

}
