package com.safaricom.pages.contactUS;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ContactUS_NewRequest {
	public AndroidDriver<MobileElement> driver;
  
  public ContactUS_NewRequest() {
	  
  }
  public ContactUS_NewRequest(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement txt_title;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/request_area_spinner")
  public AndroidElement request_area_spinner;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Domain Registration/Webhosting']")
  public AndroidElement request_area;

  @AndroidFindBy(id="com.selfcare.safaricom:id/request_sub_area_spinner")
  public AndroidElement request_sub_area_spinner;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Domain Registration']")
  public AndroidElement request_sub_area;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/requst_msg")
  public AndroidElement requst_msg;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/request_submit")
  public AndroidElement request_submit;
  
}
