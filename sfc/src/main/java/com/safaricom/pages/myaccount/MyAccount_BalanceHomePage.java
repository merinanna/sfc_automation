package com.safaricom.pages.myaccount;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyAccount_BalanceHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public MyAccount_BalanceHomePage() {
	  
  }
  public MyAccount_BalanceHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement txt_title;
  
  // Balance
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Balances']")
  public AndroidElement balance_tab_click;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data']")
  public AndroidElement data_balance_view ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement limited_data_amount;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement daily_data_amount ;
     
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga']")
  public AndroidElement bonga_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement bonga_balance ;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Airtime']")
  public AndroidElement airtime_balance_view ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement airtime_prepaid_balance ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SMS']")
  public AndroidElement sms_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement daily_sms_balance ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa']")
  public AndroidElement okoa_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement okoa_airtime_balance ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement okoa_data_balance ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Voice']")
  public AndroidElement voice_balance_view ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement voice_on_net_talktime ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement voice_off_net_talktime ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement redeemedmms_txt_amount ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_date")
  public AndroidElement redeemedmms_txt_date ;
    
}
