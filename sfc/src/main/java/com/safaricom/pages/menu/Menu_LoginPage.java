package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_LoginPage {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_LoginPage() {
	  
  }
  public Menu_LoginPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='WELCOME TO']")
  public AndroidElement login_welcome_message;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_generate_pin")
  public AndroidElement tv_generate_pin;
  }
