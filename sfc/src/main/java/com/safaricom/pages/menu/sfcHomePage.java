package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class sfcHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public sfcHomePage() {
	  
  }
  public sfcHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  
  @AndroidFindBy(className="android.widget.ImageButton")
  public AndroidElement menutab_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Log Out']")
  public AndroidElement logout_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
  public AndroidElement logout_tv_ok;
  
  @AndroidFindBy(className="android.widget.LinearLayout")
  public AndroidElement layout;
  
  @AndroidFindBy(id=" com.selfcare.safaricom:id/ll_listView")
  public AndroidElement ll_listView;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement sfc_title;
  
  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.TextView")
  public AndroidElement title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/ll_banner_image")
  public AndroidElement sfc_banner;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Check Balance']")
  public AndroidElement checkbalance_click;
   
  @AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.LinearLayout")
  public AndroidElement mpesa_click ;

  @AndroidFindBy(xpath="//android.widget.TextView[@text='Tunukiwa']")
  public AndroidElement tunukiwa_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Data Usage']")
  public AndroidElement mydataUsage_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data & SMS Plans']")
  public AndroidElement dataandsmsplans_click;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Platinum Plans']")
  public AndroidElement platinumplans_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Services']")
  public AndroidElement services_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Account']")
  public AndroidElement myaccount_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Contact Us']")
  public AndroidElement contactUs_click;
    

    
}
