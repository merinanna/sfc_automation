package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_SettingsChangeServicePin {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_SettingsChangeServicePin() {
	  
  }
  public Menu_SettingsChangeServicePin(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement changeservicepin_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_reset_pin")
  public AndroidElement changeservicepin_header;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_old_pin")
  public AndroidElement et_old_pin;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_national_id")
  public AndroidElement et_national_id;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_new_pin")
  public AndroidElement et_new_pin;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_confirm_pin")
  public AndroidElement et_confirm_pin;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_submit")
  public AndroidElement tv_submit;
  
}
