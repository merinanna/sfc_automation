package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_Netperform {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_Netperform() {
	  
  }
  public Menu_Netperform(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Network']")
  public AndroidElement netperform_network_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Speed test']")
  public AndroidElement netperform_speedtest_clicks;
  
}
