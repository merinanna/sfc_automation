package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_Netperform_Speedtest {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_Netperform_Speedtest() {
	  
  }
  public Menu_Netperform_Speedtest(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement txt_title;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/testSpeed")
  public AndroidElement testSpeed;
  
}
