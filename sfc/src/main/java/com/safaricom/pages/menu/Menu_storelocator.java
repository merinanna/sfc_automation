package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_storelocator {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_storelocator() {
	  
  }
  public Menu_storelocator(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement shoplocator_title; 
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_search_store")
  public AndroidElement edt_search_store;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/textViewGroupName")
  public AndroidElement textViewGroupName;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_name")
  public AndroidElement txt_name; 
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_address")
  public AndroidElement txt_address;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_email")
  public AndroidElement txt_email;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_weekday")
  public AndroidElement txt_weekday;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_weekend")
  public AndroidElement txt_weekend;
    
  }
