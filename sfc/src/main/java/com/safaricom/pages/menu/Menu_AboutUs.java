package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_AboutUs {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_AboutUs() {
	  
  }
  public Menu_AboutUs(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement aboutUs_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/img_app_logo")
  public AndroidElement img_app_logo;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_app_name")
  public AndroidElement app_name;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_app_version")
  public AndroidElement app_version;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_privacy_policy")
  public AndroidElement privacy_policy;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/terms_and_condn")
  public AndroidElement terms_and_condn;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/update_history")
  public AndroidElement update_history;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/ServiceCharter")
  public AndroidElement ServiceCharter;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/about_us_login_status")
  public AndroidElement about_us_login_status;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/copyright")
  public AndroidElement copyright;
  
}
