package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_tellafriend {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_tellafriend() {
	  
  }
  public Menu_tellafriend(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="android:id/title_default")
  public AndroidElement tellafriendtitle; 
  
  @AndroidFindBy(id="android:id/text1")
  public AndroidElement tellafriendicon; 
  
  }
