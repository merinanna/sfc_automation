package com.safaricom.pages.checkbalance;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DataandsmsplanservicesPage {
	public AndroidDriver<MobileElement> driver;
  
  public DataandsmsplanservicesPage() {
	  
  }
  public DataandsmsplanservicesPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement datasmsplan_title;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/indicator")
  public AndroidElement dataplanClick;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_activate")
  public AndroidElement activateClick ;

}
